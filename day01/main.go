package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "sort"
    "strconv"
)

func read(filename string) (int, error) {
    file, err := os.Open(filename)
    result := 0

    if err != nil {
        return result, err
    }

    defer file.Close()

    scanner := bufio.NewScanner(file)
    values := []int{}

    for scanner.Scan() {
        int_, err := strconv.ParseInt(scanner.Text(), 10, 32)

        if err != nil {
            return result, err
        }

        values = append(values, int(int_))
    }

    if err := scanner.Err(); err != nil {
        return result, err
    }

    sort.Ints(values)

    needle := 2020
    count := len(values)

    a := values[0]
    b := values[1]
    c := values[2]

    if a + b + c == needle {
        return a * b * c, nil
    }

    for i := 1; i < count; i++ {
        a = values[i]

        for j := i + 1; j < count; j++ {
            b = values[j]

            for k := j + 1; k < count; k++ {
                c = values[k]

                if a + b + c == needle {
                    fmt.Println(a, b, c)

                    return a * b * c, nil
                }
            }
        }
    }

    return result, fmt.Errorf("Not found")
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input]\n", filepath.Base(os.Args[0]))
        return
    }

    number, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    fmt.Println("multiplicity =", number)
}
