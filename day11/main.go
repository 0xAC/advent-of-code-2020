package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "strings"
)

type area struct {
    fields []rune
    width, length int
    rwidth, rlength int
}

type pos struct {
    c, r int
}

type kernel interface {
    cells(pos, *area)int
    limit() int
}

func newArea(width, length int) *area {
    a := area{}

    a.width = width
    a.length = length
    a.rwidth = width + 2
    a.rlength = length + 2

    count := a.rwidth * a.rlength

    fields := make([]rune, count)

    for i, _ := range fields {
        fields[i] = ' '
    }

    a.fields = fields

    return &a
}

func (a *area) copy() *area {
    c := area{}

    c.fields = make([]rune, len(a.fields))
    c.length = a.length
    c.rlength = a.rlength
    c.rwidth = a.rwidth
    c.width = a.width

    copy(c.fields, a.fields)

    return &c
}

func (a *area) get(p pos) rune {
    i := (p.r + 1) * a.rwidth + (p.c + 1)

    return a.fields[i]
}

func (a *area) set(p pos, v rune) {
    i := (p.r + 1) * a.rwidth + (p.c + 1)

    a.fields[i] = v
}

func (a *area) print() string {
    lines := []string{}

    for r := 0; r < a.rlength; r++ {
        letters := []rune{}

        for c := 0; c < a.rwidth; c++ {
            letters = append(letters, a.fields[r * a.rwidth + c])
        }

        lines = append(lines, string(letters))
    }

    return strings.Join(lines, "\n")
}

func (a *area) count(p pos, k kernel) int {
    return k.cells(p, a)
}

func (a *area) countAll(v rune) int {
    count := 0

    for c := 0; c < a.width; c++ {
        for r := 0; r < a.length; r++ {
            if a.get(pos{r: r, c: c}) == v {
                count++
            }
        }
    }

    return count
}

func (a *area) single(k kernel, p pos, clone *area) bool {
    v := clone.get(p)

    if v == '.' { return false }

    n := clone.count(p, k)

    if n == 0 && v != '#' {
        a.set(p, '#')
        return true
    }

    if n >= k.limit() && v != 'L' {
        a.set(p, 'L')
        return true
    }

    return false
}

func (a *area) run(k kernel) bool {
    clone := a.copy()
    changed := false

    for c := 0; c < a.width; c++ {
        for r := 0; r < a.length; r++ {
            if a.single(k, pos{c: c, r: r}, clone) {
                changed = true
            }
        }
    }

    return changed
}

func read(filename string) ([]string, error) {
    file, err := os.Open(filename)
    lines := []string{}

    if err != nil {
        return lines, err
    }

    scanner := bufio.NewScanner(file)

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err = scanner.Err(); err != nil {
        return lines, err
    }

    return lines, nil
}

func load(lines []string) (*area, error) {
    length := len(lines)

    if length == 0 {
        return nil, fmt.Errorf("Cannot create an area with 0 rows")
    }

    width := len(lines[0])

    if width == 0 {
        return nil, fmt.Errorf("Cannot create an area with 0 columns")
    }

    a := newArea(width, length)

    for r := 0; r < length; r++ {
        letters := []rune(lines[r])

        for c := 0; c < width; c++ {
            a.set(pos{c: c, r: r}, letters[c])
        }
    }

    return a, nil
}

type offsetKernel struct {
    offsets []pos
}

func (k *offsetKernel) cells(p pos, a *area) int {
    count := 0

    for _, o := range k.offsets {
        c := pos{ c: p.c + o.c, r: p.r + o.r }

        if a.get(c) == '#' { count++ }
    }
    
    return count
}

func (k *offsetKernel) limit() int {
    return 4
}

type dirKernel struct {
    offsets []pos
}

func (k *dirKernel) cells(p pos, a *area) int {
    count := 0

    for _, o := range k.offsets {
        c := p.c
        r := p.r

        for {
            c += o.c
            r += o.r

            if c < 0 || c > a.width - 1 { break }
            if r < 0 || r > a.length - 1 { break }

            v := a.get(pos{ c: c, r: r }) 

            if v == 'L' {
                break
            }

            if v == '#' {
                count++
                break
            }
        }
    }

    return count
}

func (k *dirKernel) limit() int {
    return 5
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Using: %s [input file]\n", filepath.Base(os.Args[0]))
        return
    }

    lines, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    a, err := load(lines)

    if err != nil {
        log.Fatal(err)
    }

    fmt.Println(a.print())

    // k := offsetKernel{[]pos{{-1,-1}, {-1,0}, {-1,1}, {1,-1}, {1,0}, {1,1}, {0,1}, {0,-1}}}
    k := dirKernel{[]pos{{-1,-1}, {-1,0}, {-1,1}, {1,-1}, {1,0}, {1,1}, {0,1}, {0,-1}}}

    for {
        changed := a.run(&k)

        if !changed { break }

        fmt.Println(a.print())
    }

    fmt.Println(a.countAll('#'))
}
