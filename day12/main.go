package main

import (
    "bufio"
    "fmt"
    "log"
    "math"
    "os"
    "path/filepath"
    "strconv"
)

func read(filename string) ([]string, error) {
    file, err := os.Open(filename)

    if err != nil {
        return nil, err
    }

    scanner := bufio.NewScanner(file)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err = scanner.Err(); err != nil {
        return nil, err
    }

    return lines, nil
}

type command interface {
    apply(*ship)
}

type moveeast struct {
    units int
}

func (m *moveeast) apply(s *ship) {
    s.waypoint.x += m.units
}

type moveforward struct {
    units int
}

func to_rad(bearing int) float64 {
    return math.Pi * float64(bearing) / 180.0
}

func (m *moveforward) apply(s *ship) {
    s.pos.x += s.waypoint.x * m.units
    s.pos.y += s.waypoint.y * m.units
}

type movenorth struct {
    units int
}

func (m *movenorth) apply(s *ship) {
    s.waypoint.y += m.units
}

type movesouth struct {
    units int
}

func (m *movesouth) apply(s *ship) {
    s.waypoint.y -= m.units
}

type movewest struct {
    units int
}

func (m *movewest) apply(s *ship) {
    s.waypoint.x -= m.units
}

type turnleft struct {
    degrees int
}

func (t *turnleft) apply(s *ship) {
    s.waypoint = s.waypoint.turn(t.degrees)
}

type turnright struct {
    degrees int
}

func (t *turnright) apply(s *ship) {
    s.waypoint = s.waypoint.turn(-t.degrees)
}

func newcommand(letter rune, value int) (command, error) {
    switch letter {
        case 'E': return &moveeast{value}, nil
        case 'F': return &moveforward{value}, nil
        case 'L': return &turnleft{value}, nil
        case 'N': return &movenorth{value}, nil
        case 'R': return &turnright{value}, nil
        case 'S': return &movesouth{value}, nil
        case 'W': return &movewest{value}, nil
        default:
            return nil, fmt.Errorf("There is not command for letter '%c'", letter)
    }
}

type position struct {
    x, y int
}

func (p position) turn(degrees int) position {
    deg := (360 + degrees) % 360

    switch deg {
        case 0:
            return p
        case 90:
            return position{x: -p.y, y: p.x}
        case 180:
            return position{x: -p.x, y: -p.y}
        case 270:
            return position{x: p.y, y: -p.x}
        default: panic("Unexpected angle")
    }
}

type ship struct {
    bearing int
    pos position
    waypoint position
}

func newship() *ship {
    return &ship{bearing: 0, pos: position{0, 0}, waypoint: position{x: 10, y: 1}}
}

func parse(lines []string) ([]command, error) {
    commands := []command{}

    for _, line := range lines {
        runes := []rune(line)
        code := runes[0]

        value, err := strconv.ParseInt(string(runes[1:]), 10, 32)

        if err != nil {
            return nil, err
        }

        command, err := newcommand(code, int(value))

        if err != nil {
            return nil, err
        }

        commands = append(commands, command)
    }

    return commands, nil
}

func abs(value int) int {
    if value < 0 {
        return -value
    }

    return value
}

func main() {
    if len(os.Args) < 2 {
        fmt.Println("Usage: %s [input file]\n", filepath.Base(os.Args[1]))
        return
    }

    lines, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    commands, err := parse(lines)

    if err != nil {
        log.Fatal(err)
    }

    s := newship()

    for _, command := range commands {
        command.apply(s)
    }

    fmt.Println(abs(s.pos.x) + abs(s.pos.y))
}
