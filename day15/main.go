package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "strconv"
    "strings"
)

func parse(filename string) ([]int, error) {
    file, err := os.Open(filename)

    if err != nil {
        return nil, err
    }

    scanner := bufio.NewScanner(file)

    for scanner.Scan() {
        tokens := strings.Split(scanner.Text(), ",")
        numbers := []int{}

        for _, t := range tokens {
            val, err := strconv.ParseInt(t, 10, 64)

            if err != nil {
                return nil, err
            }

            numbers = append(numbers, int(val))
        }

        return numbers, nil
    }


    if err = scanner.Err(); err != nil {
        return nil, err
    }

    return nil, fmt.Errorf("Cannot find any input in file %s", filename)
}

type game struct {
    cache map[int]int
    index int
    last int
    starting []int
}

func newGame(starting []int) *game {
    return &game{cache: map[int]int{}, index: 0, starting: starting}
}

func (g *game) current() int {
    if g.index < len(g.starting) {
        return g.starting[g.index]
    }

    before, ok := g.cache[g.last]

    if !ok { return 0 }

    return g.index - before - 1
}

func (g *game) updateCache() {
    if g.index == 0 { return }

    g.cache[g.last] = g.index - 1
}

func (g *game) next() int {
    number := g.current()
    g.updateCache()
    g.last = number

    g.index++
    return number
}

func main() {
    if len(os.Args) < 2 {
        fmt.Println("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
        return
    }

    numbers, err := parse(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    g := newGame(numbers)
    number := 0

    for i:= 0; i < 30000000; i++ {
        number = g.next()
    }

    fmt.Println(number)

}
