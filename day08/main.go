package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "regexp"
    "strconv"
)

type state struct {
    acc int
    ip int
}

type op interface {
    Exec(*state)
    Code() string
}

type inv interface {
    Inv() op
    op
}

func read(filename string) ([]string, error) {
    file, err := os.Open(filename)
    lines := []string{}

    if err != nil {
        return lines, err
    }

    scanner := bufio.NewScanner(file)

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err = scanner.Err(); err != nil {
        return lines, err
    }

    return lines, nil
}

type acc struct {
    v int
}

type jmp struct {
    v int
}

type nop struct {
    v int
}

func (a acc)Exec(s *state) {
    s.acc += a.v
    s.ip++
}

func (a acc)Code() string {
    return "acc"
}

func (j jmp)Exec(s *state) {
    s.ip += j.v
}

func (j jmp)Code() string {
    return "jmp"
}

func (j jmp)Inv() op {
    return nop{j.v}
}

func (n nop)Exec(s *state) {
    s.ip++
}

func (n nop)Code() string {
    return "nop"
}

func (n nop)Inv() op {
    return jmp{n.v}
}

var opRegexp, _ = regexp.Compile("^(\\w+) \\+?(\\-?\\d+)$")
func parse(lines []string) ([]op, error) {
    ops := []op{}

    for _, line := range lines {
        groups := opRegexp.FindStringSubmatch(line)
        var o op
        val, err := strconv.ParseInt(groups[2], 10, 32)

        if err != nil {
            return ops, err
        }

        v := int(val)

        switch groups[1] {
        case "acc":
            o = acc{v}
        case "nop":
            o = nop{v}
        case "jmp":
            o = jmp{v}
        }

        ops = append(ops, o)
    }

    return ops, nil
}

func run(ops []op) (int, bool) {
    s := state{0, 0}
    exec := map[int]bool{}

    for s.ip < len(ops) {
        if _, ok := exec[s.ip]; ok {
            return s.acc, false
        }

        exec[s.ip] = true
        o := ops[s.ip]

        o.Exec(&s)
    }

    return s.acc, true
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
        return
    }

    lines, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    ops, err := parse(lines)

    if err != nil {
        log.Fatal(err)
    }

    a := 0

    for i := 0; i < len(ops); i++ {
        inv_, ok := ops[i].(inv)

        if !ok {
            continue
        }

        ops[i] = inv_.Inv()

        if a, ok = run(ops); ok {
            break
        }

        ops[i] = inv_
    }

    fmt.Println(a)
}
