package main

import "testing"

type helper struct {
    t *testing.T
}

func (h helper) parse(lines []string) *game {
    t := h.t

    parser, err := newParser()

    if err != nil { t.Error(err) }

    game, err := parser.parse(lines)

    if err != nil { t.Error(err) }

    return game
}

func TestParserHappy(t *testing.T) {
    h := helper{t}

    actGame := h.parse([]string{
        "Player 1:",
        "9",
        "2",
        "6",
        "3",
        "1",
        "",
        "Player 2:",
        "5",
        "8",
        "4",
        "7",
        "10",
    })

    if actGame == nil { return }

    expGame := newGame(
        deck{9, 2, 6, 3, 1},
        deck{5, 8, 4, 7, 10},
    )

    if !expGame.equals(actGame) {
        t.Errorf("Expected\n%s\nbut got\n%s", expGame, actGame)
    }
}

func TestPart1Round1(t *testing.T) {
    ga := newGame(
        deck{9, 2, 6, 3, 1},
        deck{5, 8, 4, 7, 10},
    )

    ga.round1()

    expected := newGame(
        deck{2, 6, 3, 1, 9, 5},
        deck{8, 4, 7, 10},
    )

    if !expected.equals(ga) {
        t.Errorf("Expected\n%s\nbut got\n%s", expected, ga)
    }
}

func TestPart1Round2(t *testing.T) {
    ga := newGame(
        deck{9, 2, 6, 3, 1},
        deck{5, 8, 4, 7, 10},
    )

    for i := 0; i < 2; i++ { ga.round1() }

    expected := newGame(
        deck{6, 3, 1, 9, 5},
        deck{4, 7, 10, 8, 2},
    )

    if !expected.equals(ga) {
        t.Errorf("Expected\n%s\nbut got\n%s", expected, ga)
    }
}

func TestPart1Play(t *testing.T) {
    ga := newGame(
        deck{9, 2, 6, 3, 1},
        deck{5, 8, 4, 7, 10},
    )

    score := ga.play1()

    expGa := newGame(
        deck{},
        deck{3, 2, 10, 6, 8, 5, 9, 4, 7, 1},
    )

    if !expGa.equals(ga) {
        t.Errorf("Expected\n%s\nbut got\n%s", expGa, ga)
    }

    expScore := 306

    if score != expScore {
        t.Errorf("Expected %d but got %d", expScore, score)
    }
}

func TestPart2Round1(t *testing.T) {
    ga := newGame(
        deck{9, 2, 6, 3, 1},
        deck{5, 8, 4, 7, 10},
    )

    ga.round2()

    expected := newGame(
        deck{2, 6, 3, 1, 9, 5},
        deck{8, 4, 7, 10},
    )

    if !expected.equals(ga) {
        t.Errorf("Expected\n%s\nbut got\n%s", expected, ga)
    }
}

func TestPart2Rounds(t *testing.T) {
    ga := newGame(
        deck{9, 2, 6, 3, 1},
        deck{5, 8, 4, 7, 10},
    )

    games := []game{
        game{
            fst: deck{2, 6, 3, 1, 9, 5},
            snd: deck{8, 4, 7, 10},
        },
        game{
            fst: deck{6, 3, 1, 9, 5},
            snd: deck{4, 7, 10, 8, 2},
        },
        game{
            fst: deck{3, 1, 9, 5, 6, 4},
            snd: deck{7, 10, 8, 2},
        },
        game{
            fst: deck{1, 9, 5, 6, 4},
            snd: deck{10, 8, 2, 7, 3},
        },
        game{
            fst: deck{9, 5, 6, 4},
            snd: deck{8, 2, 7, 3, 10, 1},
        },
        game{
            fst: deck{5, 6, 4, 9, 8},
            snd: deck{2, 7, 3, 10, 1},
        },
        game{
            fst: deck{6, 4, 9, 8, 5, 2},
            snd: deck{7, 3, 10, 1},
        },
        game{
            fst: deck{4, 9, 8, 5, 2},
            snd: deck{3, 10, 1, 7, 6},
        },
        // Game 2
        game{
            fst: deck{9, 8, 5, 2},
            snd: deck{10, 1, 7},
        },
        game{
            fst: deck{8, 5, 2},
            snd: deck{1, 7, 10, 9},
        },
        game{
            fst: deck{5, 2, 8, 1},
            snd: deck{7, 10, 9},
        },
        game{
            fst: deck{2, 8, 1},
            snd: deck{10, 9, 7, 5},
        },
        game{
            fst: deck{8, 1},
            snd: deck{9, 7, 5, 10, 2},
        },
        game{
            fst: deck{1},
            snd: deck{7, 5, 10, 2, 9, 8},
        },
        // Game 1
        game{
            fst: deck{9, 8, 5, 2},
            snd: deck{10, 1, 7, 6, 3, 4},
        },
        game{
            fst: deck{8, 5, 2},
            snd: deck{1, 7, 6, 3, 4, 10, 9},
        },
        game{
            fst: deck{5, 2, 8, 1},
            snd: deck{7, 6, 3, 4, 10, 9},
        },
        game{
            fst: deck{2, 8, 1},
            snd: deck{6, 3, 4, 10, 9, 7, 5},
        },
        // Game 3
        game{
            fst: deck{8, 1},
            snd: deck{3, 4, 10, 9, 7, 5},
        },
        game{
            fst: deck{1, 8, 3},
            snd: deck{4, 10, 9, 7, 5},
        },
        game{
            fst: deck{8},
            snd: deck{10, 9, 7, 5},
        },
        game{
            fst: deck{8, 3},
            snd: deck{10, 9, 7, 5, 4, 1},
        },
        game{
            fst: deck{3},
            snd: deck{9, 7, 5, 4, 1, 10, 8},
        },
        // Game 1
        game{
            fst: deck{8, 1},
            snd: deck{3, 4, 10, 9, 7, 5, 6, 2},
        },
        game{
            fst: deck{1, 8, 3},
            snd: deck{4, 10, 9, 7, 5, 6, 2},
        },
        // Game 5
        game{ fst: deck{8},
            snd: deck{10, 9, 7, 5},
        },
        game{
            fst: deck{8, 3},
            snd: deck{10, 9, 7, 5, 6, 2, 4, 1},
        },
        game{
            fst: deck{3},
            snd: deck{9, 7, 5, 6, 2, 4, 1, 10, 8},
        },
    }

    for i, expGa := range games {
        ga.round2()

        if !expGa.equals(ga) {
            t.Errorf("Round %d, Expected\n%s\nbut got\n%s", i + 2, &expGa, ga)
            return
        }
    }
}

func TestPart2Play(t *testing.T) {
    ga := newGame(
        deck{9, 2, 6, 3, 1},
        deck{5, 8, 4, 7, 10},
    )

    score := ga.play2()
    expScore := 291

    if score != expScore {
        t.Errorf("Expected %d but got %d", expScore, score)
    }
}

func TestPart2Infinity(t *testing.T) {
    ga := newGame(
        deck{43, 19},
        deck{2, 29, 14},
    )

    score := ga.play2()
    expScore := 105

    if score != expScore {
        t.Errorf("Expected %d but got %d", expScore, score)
    }
}
