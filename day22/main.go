package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "regexp"
    "strconv"
    "strings"
)

type deck []int

func (d deck) equals(o deck) bool {
    if len(d) != len(o) { return false }

    for i, card := range d {
        if card != o[i] { return false }
    }

    return true
}

type row struct {
    fst, snd deck
}

func (r row) hash() int {
    return score(r.fst) * score(r.snd)
}

func (r row) equals(o row) bool {
    return r.fst.equals(o.fst) && r.snd.equals(o.snd)
}

type cache struct {
    buckets map[int][]row
}

func newCache() cache {
    return cache{buckets: map[int][]row{}}
}

func (c *cache) add(fst, snd deck) {
    r := row{fst, snd}
    key := r.hash()

    rows, ok := c.buckets[key]

    if !ok { rows = []row{} }

    for _, s := range rows {
        if s.equals(r) { return }
    }

    c.buckets[key] = append(rows, row{fst, snd})
}

func (c *cache) has(fst, snd deck) bool {
    r := row{fst, snd}
    key := r.hash()

    rows, ok := c.buckets[key]

    if !ok { return false }

    for _, s := range rows {
        if s.equals(r) {
            return true
        }
    }

    return false
}

type parser struct {
    numberRegexp *regexp.Regexp
}

type frame struct {
    fst, snd deck
    fstCard, sndCard int
    memory cache
}

type game struct {
    fst, snd deck
    stack []frame
    memory cache
}

func (g *game) push(fstCard, sndCard int) {
    fst := make(deck, len(g.fst))
    snd := make(deck, len(g.snd))

    copy(fst, g.fst)
    copy(snd, g.snd)

    fr := frame{fst, snd, fstCard, sndCard, g.memory}

    g.stack = append(g.stack, fr)

    g.fst = g.fst[:fstCard]
    g.snd = g.snd[:sndCard]
    g.memory = newCache()
}

func (g *game) pop() (int, int) {
    fr := g.stack[len(g.stack)-1]

    g.fst = fr.fst
    g.snd = fr.snd

    fstCard := fr.fstCard
    sndCard := fr.sndCard

    g.stack = g.stack[:len(g.stack)-1]
    g.memory = fr.memory

    return fstCard, sndCard
}

func (g *game) post() {
    if len(g.fst) == 0 && len(g.stack) > 0 {
        fstCard, sndCard := g.pop()
        g.snd = append(g.snd, sndCard, fstCard)
        return
    }

    if len(g.snd) == 0 && len(g.stack) > 0 {
        fstCard, sndCard := g.pop()
        g.fst = append(g.fst, fstCard, sndCard)
    }
}

func (g *game) round2() {
    if g.remind() {
        g.snd = deck{}
        g.post()
        return
    }

    g.remember()

    fstCard := g.fst[0]
    sndCard := g.snd[0]

    g.snd = g.snd[1:len(g.snd)]
    g.fst = g.fst[1:len(g.fst)]

    if len(g.fst) > fstCard - 1 && len(g.snd) > sndCard - 1 {
        g.push(fstCard, sndCard)
        return
    }

    if fstCard > sndCard {
        g.fst = append(g.fst, fstCard, sndCard)
    } else {
        g.snd = append(g.snd, sndCard, fstCard)
    }

    g.post()
}

func (g *game) round1() {
    fstCard := g.fst[0]
    sndCard := g.snd[0]

    if fstCard > sndCard {
        g.fst = append(g.fst[1:], fstCard, sndCard)
        g.snd = g.snd[1:len(g.snd)]
    } else {
        g.fst = g.fst[1:len(g.fst)]
        g.snd = append(g.snd[1:], sndCard, fstCard)
    }
}

func score(deck deck) int {
    weight := len(deck)
    score := 0

    for i, card := range deck {
        score += card * (weight - i)
    }

    return score
}

func (g *game) play1() int {
    for {
        if len(g.fst) == 0 { return score(g.snd) }
        if len(g.snd) == 0 { return score(g.fst) }

        g.round1()
    }
}

func (g *game) remember() {
    g.memory.add(g.fst, g.snd)
}

func (g *game) remind() bool {
    return g.memory.has(g.fst, g.snd)
}

func newGame(fst, snd deck) *game {
    return  &game{fst, snd, []frame{}, newCache()}
}

func (g *game) play2() int {
    for {
        if len(g.fst) == 0 { return score(g.snd) }
        if len(g.snd) == 0 { return score(g.fst) }

        g.round2()
    }
}

func (g *game) printPlayer(deck deck, no int) []string {
    lines := []string{fmt.Sprintf("Player %d:", no)}

    for _, card := range deck {
        lines = append(lines, fmt.Sprintf("%d", card))
    }

    return lines
}

func (g *game) String() string {
    lines := g.printPlayer(g.fst, 1)
    lines = append(lines, "")
    lines = append(lines, g.printPlayer(g.snd, 2)...)

    return strings.Join(lines, "\n")
}

func (g *game) equals(o *game) bool {
    if o == nil { return false }

    if len(o.snd) != len(o.snd) { return false }

    for i, card := range g.snd {
        if card != o.snd[i] { return false }
    }

    return true
}

func (p *parser) parsePlayer(lines []string, no int) (deck, error){
    header := fmt.Sprintf("Player %d:", no)
    if lines[0] != header {
        return nil, fmt.Errorf("Expected '%s', but got '%s'", header, lines[0])
    }

    cards := deck{}
    for _, line := range lines[1:] {
        if !p.numberRegexp.MatchString(line) { break }

        value, err := strconv.ParseInt(line, 10, 64)

        if err != nil { return nil, err }

        cards = append(cards, int(value))
    }

    return cards, nil
}

func (p *parser) parse(lines []string) (*game, error) {
    fst, err := p.parsePlayer(lines, 1)

    if err != nil { return nil, err }

    snd, err := p.parsePlayer(lines[len(fst)+2:], 2)

    if err != nil { return nil, err }

    return newGame(fst, snd), nil
}

func newParser() (*parser, error) {
    numberRegexp, err := regexp.Compile("^\\d+$")
    if err != nil { return nil, err }

    return &parser{numberRegexp}, nil
}

func read(filename string) ([]string, error) {
    file, err := os.Open(filename)

    if err != nil { return nil, err }

    scanner := bufio.NewScanner(file)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        return nil, err
    }

    return lines, nil
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[1]))
        return
    }

    lines, err := read(os.Args[1])

    if err != nil { log.Fatal(err) }

    parser, err := newParser()

    if err != nil { log.Fatal(err) }

    ga, err := parser.parse(lines)

    if err != nil { log.Fatal(err) }

    fmt.Println("part 1:", ga.play1())

    ga, err = parser.parse(lines)

    if err != nil { log.Fatal(err) }

    fmt.Println("part 2:", ga.play2())
}
