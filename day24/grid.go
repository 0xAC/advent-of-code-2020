package main

type grid map[coords]bool

func newGrid(positions []coords) grid {
	counts := map[coords]int{}

	for _, pos := range positions {
		count, ok := counts[pos]

		if !ok {
			count = 0
		}

		counts[pos] = count + 1
	}

	gr := grid{}

	for pos, count := range counts {
		if count%2 != 1 {
			continue
		}
		gr[pos] = true
	}

	return gr
}

func (g grid) countBlack() int {
	count := 0

	for _, color := range g {
		if color == black {
			count++
		}
	}

	return count
}

var adjacents = []coords{
	e,
	se,
	sw,
	w,
	nw,
	ne,
}

func (g grid) countAdjacent(pos coords) (count int) {
	for _, off := range adjacents {
		color, ok := g[pos.add(off)]

		if !ok || color != black {
			continue
		}

		count++
	}

	return
}

func (g grid) size() (min coords, max coords) {
	set := false

	for pos, _ := range g {
		if !set {
			min = pos
			max = pos
			set = true
			continue
		}

		for i := 0; i < len(pos); i++ {
			if pos[i] < min[i] {
				min[i] = pos[i]
				continue
			}

			if pos[i] > max[i] {
				max[i] = pos[i]
			}
		}
	}

	return
}

func (g grid) turn() {
	flips := []coords{}
	min, max := g.size()

	for y := min[1] - 1; y <= max[1]+1; y++ {
		for x := min[0] - 1; x <= max[0]+1; x++ {
			pos := coords{x, y}
			color := g[pos]

			if color != black && color != white {
				panic("color must be either white or black")
			}

			count := g.countAdjacent(pos)

			if color == black && count != 1 && count != 2 {
				flips = append(flips, pos)
				continue
			}

			if color == white && count == 2 {
				flips = append(flips, pos)
				continue
			}
		}
	}

	for _, pos := range flips {
		color := g[pos]

		if color == white {
			g[pos] = black
			continue
		}

		delete(g, pos)
	}
}

func (g grid) similate(days int) grid {
	for i := 0; i < days; i++ {
		g.turn()
	}

	return g
}
