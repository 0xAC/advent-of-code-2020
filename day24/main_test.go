package main

import "testing"

func TestParse0(t *testing.T) {
	pos, err := parseLine("nwwswee")

	if err != nil {
		t.Error(err)
		return
	}

	expPos := coords{0, 0}

	if expPos != pos {
		t.Errorf("Expected position to be %s but got %s", expPos, pos)
	}
}

func TestParse1(t *testing.T) {
	pos, err := parseLine("esew")

	if err != nil {
		t.Error(err)
		return
	}

	expPos := coords{0, -1}

	if expPos != pos {
		t.Errorf("Expected %s but got %s", expPos, pos)
	}
}

func testGrid() (grid, error) {
	input := []string{
		"sesenwnenenewseeswwswswwnenewsewsw",
		"neeenesenwnwwswnenewnwwsewnenwseswesw",
		"seswneswswsenwwnwse",
		"nwnwneseeswswnenewneswwnewseswneseene",
		"swweswneswnenwsewnwneneseenw",
		"eesenwseswswnenwswnwnwsewwnwsene",
		"sewnenenenesenwsewnenwwwse",
		"wenwwweseeeweswwwnwwe",
		"wsweesenenewnwwnwsenewsenwwsesesenwne",
		"neeswseenwwswnwswswnw",
		"nenwswwsewswnenenewsenwsenwnesesenew",
		"enewnwewneswsewnwswenweswnenwsenwsw",
		"sweneswneswneneenwnewenewwneswswnese",
		"swwesenesewenwneswnwwneseswwne",
		"enesenwswwswneneswsenwnewswseenwsese",
		"wnwnesenesenenwwnenwsewesewsesesew",
		"nenewswnwewswnenesenwnesewesw",
		"eneswnwswnwsenenwnwnwwseeswneewsenese",
		"neswnwewnwnwseenwseesewsenwsweewe",
		"wseweeenwnesenwwwswnew",
	}

	lines := []coords{}

	for _, line := range input {
		flips, err := parseLine(line)

		if err != nil {
			return nil, err
		}

		lines = append(lines, flips)
	}

	return newGrid(lines), nil
}

func TestCountBlack(t *testing.T) {
	gr, err := testGrid()

	if err != nil {
		t.Error(err)
		return
	}

	count := gr.countBlack()
	expCount := 10

	if expCount != count {
		t.Errorf("Expected counts to be %d but got %d", expCount, count)
	}
}

func TestSimulate(t *testing.T) {
	gr, err := testGrid()

	if err != nil {
		t.Error(err)
		return
	}

	count := gr.similate(100).countBlack()
	expCount := 2208

	if expCount != count {
		t.Errorf("Expected counts to be %d but got %d", expCount, count)
	}
}
