package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
)

type coords [2]int

func (c coords) add(b coords) coords {
	return coords{c[0] + b[0], c[1] + b[1]}
}

func (c coords) String() string {
	return fmt.Sprintf("(%d %d)", c[0], c[1])
}

func parseLong(input string, col int, e, w coords) (coords, error) {
	chr := input[col]
	var err error

	var co coords

	switch chr {
	case 'e':
		co = e
	case 'w':
		co = w
	default:
		err = fmt.Errorf("Expected 'e' or 'w' but got '%c' at position %d", chr, col)
	}

	return co, err
}

func parseLine(input string) (coords, error) {
	pos := coords{}

	for i := 0; i < len(input); i++ {
		var err error
		var co coords

		switch input[i] {
		case 'n':
			i++
			co, err = parseLong(input, i, ne, nw)
		case 's':
			i++
			co, err = parseLong(input, i, se, sw)
		default:
			co, err = parseLong(input, i, e, w)
		}

		if err != nil {
			return pos, err
		}

		pos = pos.add(co)
	}

	return pos, nil
}

var (
	e  = coords{1, 0}
	se = coords{0, -1}
	sw = coords{-1, -1}
	w  = coords{-1, 0}
	nw = coords{0, 1}
	ne = coords{1, 1}
)

func readLines(filename string) ([]coords, error) {
	file, err := os.Open(filename)

	if err != nil {
		return nil, err
	}

	scanner := bufio.NewScanner(file)
	positions := []coords{}

	for scanner.Scan() {
		pos, err := parseLine(scanner.Text())

		if err != nil {
			return nil, err
		}

		positions = append(positions, pos)
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return positions, nil
}

const (
	black = true
	white = false
)

func abs(value int) int {
	if value < 0 {
		return -value
	}

	return value
}

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
		return
	}

	lines, err := readLines(os.Args[1])

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("part 1:", newGrid(lines).countBlack())
	fmt.Println("part 2:", newGrid(lines).similate(100).countBlack())
}
