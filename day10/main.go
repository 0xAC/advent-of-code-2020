package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "sort"
    "strconv"
)

func parseInt(input string) (int, error) {
    val, err := strconv.ParseInt(input, 10, 32)

    if err != nil {
        return 0, err
    }

    return int(val), nil
}

func read(filename string) ([]int, error) {
    file, err := os.Open(filename)
    adapters := []int{}

    if err != nil {
        return adapters, err
    }

    scanner := bufio.NewScanner(file)

    for scanner.Scan() {
        val, err := parseInt(scanner.Text())

        if err != nil {
            return adapters, err
        }

        adapters = append(adapters, val)
    }

    if err = scanner.Err(); err != nil {
        return adapters, err
    }

    return adapters, nil
}

func max(adapters []int) (int, error) {
    if len(adapters) == 0 {
        return 0, fmt.Errorf("Empty list of adapters")
    }

    maxAdapt := adapters[0]

    for _, adapt := range adapters[1:] {
        if adapt > maxAdapt { maxAdapt = adapt }
    }

    return maxAdapt, nil
}

func countConnections(adapters []int) map[int]int {
    out := map[int]int{}

    for i, adapt := range adapters[1:] {
        diff := adapt - adapters[i]

        val, ok := out[diff]

        if !ok { val = 0 }

        out[diff] = val + 1
    }

    return out
}

func countCombinations(i int, adapters []int, cache map[int]int) int {
    if v, ok := cache[i]; ok {
        return v
    }

    if i == len(adapters) - 1 {
        return 1
    }

    cur := adapters[i]
    count := 0

    for j, v := range adapters[i+1:] {
        if v - cur < 4 {
            count += countCombinations(i + j + 1, adapters, cache)
        }
    }

    cache[i] = count
    return count
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
        return
    }

    adapters, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    sort.Ints(adapters)

    outlet := 0
    builtinAdapt, err := max(adapters)
    builtinAdapt += 3

    if err != nil {
        log.Fatal(err)
    }

    chain := append(append([]int{outlet}, adapters...), builtinAdapt)
    fmt.Println(countCombinations(0, chain, map[int]int{}))
}

