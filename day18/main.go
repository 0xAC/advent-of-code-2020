package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "strconv"
    "strings"
)

func read(filename string) ([]string, error) {
    file, err := os.Open(filename)

    if err != nil {
        return nil, err
    }

    scanner := bufio.NewScanner(file)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err = scanner.Err(); err != nil {
        return nil, err
    }

    return lines, nil
}

type expr interface {
    eval() int
}

type literal struct {
    value int
}

func (l literal) String() string {
    return fmt.Sprintf("%d", l)
}

func (l literal) eval() int {
    return l.value
}

type add struct {
    left, right expr
}

func (a add) eval() int {
    return a.left.eval() + a.right.eval()
}

func (a add) String() string {
    return fmt.Sprintf("(%s + %s)", a.left, a.right)
}

type mul struct {
    left, right expr
}

func (m mul) eval() int {
    return m.left.eval() * m.right.eval()
}

func (m mul) String() string {
    return fmt.Sprintf("(%s * %s)", m.left, m.right)
}

func scanNumber(input []rune) string {
    text := []rune{}

    for _, chr := range input {
        if !(chr >= '0' && chr <= '9') { break }

        text = append(text, chr)
    }

    return string(text)
}

type atom struct {
    kind rune
    value int 
}

func scan(rawInput string) ([]atom, error) {
    input := []rune(strings.ReplaceAll(rawInput, " ", ""))

    atoms := []atom{}
    i := 0

    for i < len(input) {
        chr := input[i]

        if chr >= '0' && chr <= '9' {
            text := scanNumber(input[i:])

            value, err := strconv.ParseInt(text, 10, 64)

            if err != nil {
                return nil, err
            }

            atoms = append(atoms, atom{kind: 'd', value: int(value)})
            i += len(text)
            continue
        }

        atoms = append(atoms, atom{kind: chr})
        i++
    }

    return atoms, nil
}

type parser struct {
    atoms []atom
    i int
}

func (p *parser) parsePrimary() expr {
    at := p.atoms[p.i]

    if at.kind == '(' {
        p.i++
        ex := p.parseMul()
        p.i++

        return ex
    }

    ex := literal{value: at.value}
    p.i++
    return ex
}

func (p *parser) parseAdd() expr {
    left := p.parsePrimary()

    if p.i == len(p.atoms) { return left }

    at := p.atoms[p.i]

    if at.kind != '+' { return left }

    p.i++

    return add{left, p.parseAdd()}
}

func (p *parser) parseMul() expr {
    left := p.parseAdd()

    if p.i == len(p.atoms) { return left }

    at := p.atoms[p.i]

    if at.kind != '*' { return left }

    p.i++

    return mul{left, p.parseMul()}
}

func parse(atoms []atom) expr {
    p := &parser{atoms: atoms, i: 0}

    return p.parseMul()
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
        return
    }

    lines, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    sum := 0
    for _, line := range lines {
        atoms, err := scan(line)

        if err != nil {
            log.Fatal(err)
        }

        sum += parse(atoms).eval()
    }

    fmt.Println(sum)
}
