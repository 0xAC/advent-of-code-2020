package main

import(
   "bufio"
   "fmt"
   "log"
   "os"
   "path/filepath"
)

func mapRow(line string) map[rune]bool {
    out := map[rune]bool{}

    for _, c := range line {
        out[c] = true
    }

    return out
}

func copyMap(in map[rune]bool) map[rune]bool {
    out := map[rune]bool{}

    for k, v := range in {
        out[k] = v
    }

    return out
}

func aggregate1(rows []map[rune]bool) map[rune]bool {
    out := map[rune]bool{}

    for _, row := range rows {
        for key, val := range row {
            out[key] = val
        }
    }

    return out
}

func printMap(row map[rune]bool) string {
    out := ""

    for k, v := range row {
        out += fmt.Sprintf("%s = %t, ", string([]rune{k}), v)
    }

    return out
}

func aggregate2(rows []map[rune]bool) map[rune]bool {
    out := copyMap(rows[0])

    for key, _ := range out {
        for _, row := range rows[1:] {
            other, ok := row[key]

            if key == 'w' {
                fmt.Println(other, ok)
            }

            if ok {
                out[key] = out[key] && other
                continue
            }

            out[key] = false
        }
    }

    return out
}

func count(row map[rune]bool) int {
    sum := 0
    letters := []rune{}

    for k, val := range row {
        if val {
            letters = append(letters, k)
            sum++
        }
    }

    fmt.Println(sum, ":", string(letters))
    return sum
}

func read(filename string, aggregate func([]map[rune]bool)map[rune]bool) (int, error) {
    file, err := os.Open(filename)
    sum := 0

    if err != nil {
        return sum, err
    }

    scanner := bufio.NewScanner(file)
    buffer := []map[rune]bool{}

    for scanner.Scan() {
        line := scanner.Text()

        if line == "" {
            sum += count(aggregate(buffer))
            buffer = []map[rune]bool{}
            continue
        }

        fmt.Println(line)
        buffer = append(buffer, mapRow(scanner.Text()))
    }

    sum += count(aggregate(buffer))

    if err = scanner.Err(); err != nil {
        return sum, err
    }

    return sum, nil
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
        return
    }

    c, err := read(os.Args[1], aggregate2)

    if err != nil {
        log.Fatal(err)
    }

    fmt.Println(c)
}
