package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "strconv"
)

func read(filepath string) ([]int64, error) {
    file, err := os.Open(filepath)
    nums := []int64{}

    if err != nil {
        return nums, err
    }

    scanner := bufio.NewScanner(file)

    for scanner.Scan() {
        val, err := strconv.ParseInt(scanner.Text(), 10, 64)

        if err != nil {
            return nums, err
        }

        nums = append(nums, val)
    }

    if err = scanner.Err(); err != nil {
        return nums, err
    }

    return nums, err
}

func pair(needle int64, stack[]int64) bool {
    for i, u := range stack {
        for j := i + 1; j < len(stack); j++ {
            v := stack[j]

            if u + v == needle {
                return true
            }
        }
    }

    return false
}

func detect(nums []int64, size int) (int64, error) {
    for i := size; i < len(nums); i++ {
        val := nums[i]

        if pair(val, nums[i-size:i]) {
            continue
        }

        return val, nil
    }

    return 0, fmt.Errorf("The number was not found")
}

func sumMinMax(nums []int64) int64 {
    min := nums[0]
    max := nums[0]

    for _, v := range nums[1:] {
        if v < min {
            min = v
        }

        if v > max {
            max = v
        }
    }

    return min + max
}

func crack(nums []int64, needle int64) (int64, error) {
    i := 0
    j := 1
    sum := int64(nums[0])

    for sum != needle {
        if j > len(nums) {
            return sum, fmt.Errorf("Sequence not found")
        }

        if sum < needle {
            j++
            sum += nums[j-1]
            continue
        }

        i++

        if i == j {
            j = i + 1
            sum = nums[i]
            continue
        }

        sum -= nums[i-1]
    }

    return sumMinMax(nums[i:j]), nil
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
        return
    }

    nums, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    val, err := detect(nums, 25)

    if err != nil {
        log.Fatal(err)
    }

    val, err = crack(nums, val)

    if err != nil {
        log.Fatal(err)
    }

    fmt.Println(val)
}
