package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "regexp"
    "sort"
    "strings"
)

var foodRegexp, _ = regexp.Compile("^(\\w+( \\w+)*) \\(contains (\\w+(, \\w+)*)\\)$")

func read(filename string) ([]food, error) {
    file, err := os.Open(filename)

    if err != nil { return nil, err }

    scanner := bufio.NewScanner(file)
    foods := []food{}

    for scanner.Scan() {
        groups := foodRegexp.FindStringSubmatch(scanner.Text())

        if groups == nil {
            return nil, fmt.Errorf("Unexpected line '%s'", scanner.Text())
        }

        allergens := strings.Split(groups[3], ", ")
        ingredients := strings.Split(groups[1], " ")
        foods = append(foods, food{allergens: allergens, ingredients: ingredients})
    }

    if err := scanner.Err(); err != nil {
        return nil, err
    }

    return foods, nil
}

type food struct {
    allergens []string
    ingredients []string
}

func (f food) String() string {
    allergens := strings.Join(f.allergens, ", ")
    ingredients := strings.Join(f.ingredients, " ")
    return fmt.Sprintf("%s (contains %s)", ingredients, allergens)
}

func eliminateIngredient(dict map[string][]string, ingredient string) map[string][]string {
    for allergen, ingredients := range dict {
        dict[allergen] = remove(ingredients, ingredient)
    }

    return dict
}

type poison struct {
    allergen, ingredient string
}

type poisons []poison

func (p poisons) Len() int {
    return len(p)
}

func (p poisons) Less(i, j int) bool {
    return p[i].allergen < p[j].allergen
}

func (p poisons) Swap(i, j int) {
    p[i], p[j] = p[j], p[i]
}

func (p poisons) Join(glue string) string {
    ingredients := []string{}

    for _, it := range p {
        ingredients = append(ingredients, it.ingredient)
    }

    return strings.Join(ingredients, glue)
}

func eliminateAllergen(dict map[string][]string, eliminated poisons) poisons {
    for allergen, ingredients := range dict {
        if len(ingredients) != 1 { continue }

        delete(dict, allergen)

        pr := poison{allergen: allergen, ingredient: ingredients[0]}
        eliminated = append(eliminated, pr)

        return eliminateAllergen(eliminateIngredient(dict, ingredients[0]), eliminated)
    }

    return eliminated
}

func eliminateAllergens(foods []food) ([]food, poisons) {
    dict := map[string][]string{}

    for _, fo := range foods {
        for _, allergen := range fo.allergens {
            ingredients, ok := dict[allergen]

            if !ok {
                ingredients = make([]string, len(fo.ingredients))
                copy(ingredients, fo.ingredients)
                dict[allergen] = ingredients
                continue
            }

            for _, ingredient := range ingredients {
                if includes(fo.ingredients, ingredient) { continue }

                ingredients = remove(ingredients, ingredient)
            }

            dict[allergen] = ingredients
        }
    }

    eliminated := eliminateAllergen(dict, poisons{})
    allergicIngredientsDict := map[string]int{}

    for _, pr := range eliminated {
        allergicIngredientsDict[pr.ingredient] = 0
    }

    allergenFreeFoods := []food{}

    for _, fo := range foods {
        allergenFreeIngredients := []string{}

        for _, ingredient := range fo.ingredients {
            if _, ok := allergicIngredientsDict[ingredient]; ok { continue }

            allergenFreeIngredients = append(allergenFreeIngredients, ingredient)
        }

        fo.ingredients = allergenFreeIngredients
        fo.allergens = []string{}
        allergenFreeFoods = append(allergenFreeFoods, fo)
    }

    return allergenFreeFoods, eliminated
}

func includes(stack []string, needle string) bool {
    for _, item := range stack {
        if item == needle { return true }
    }

    return false
}

func remove(items []string, item string) []string {
    list := []string{}

    for _, v := range items {
        if v == item { continue }

        list = append(list, v)
    }

    return list
}

func uniqIngredients(foods []food) []string {
    ingredientsDict := map[string]byte{}

    for _, fo := range foods {
        for _, ingredient := range fo.ingredients {
            ingredientsDict[ingredient] = 0
        }
    }

    ingredients := []string{}

    for ingredient, _ := range ingredientsDict {
        ingredients = append(ingredients, ingredient)
    }

    sort.Strings(ingredients)

    return ingredients
}

func countIngredient(foods []food, ingredient string) int {
    count := 0

    for _, fo := range foods {
        if includes(fo.ingredients, ingredient) {
            count++
        }
    }

    return count
}

func clone(foods []food) []food {
    clone := []food{}

    for _, fo := range foods {
        clone = append(clone, fo)
    }

    return clone
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[1]))
        return
    }

    foods, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    foods, eliminated := eliminateAllergens(foods)

    count := 0
    for _, ingredient := range uniqIngredients(foods) {
        count += countIngredient(foods, ingredient)
    }

    fmt.Println("part 1:", count)

    sort.Sort(eliminated)
    fmt.Println(eliminated.Join(","))
}
