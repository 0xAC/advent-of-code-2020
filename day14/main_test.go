package main

import (
    "sort"
    "testing"
)

func TestParseMask(t *testing.T) {
    actualMask := parseMask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X")
    expectedMask := mask{
        filter: 0b111111111111111111111111111110111101,
        value: 0b000000000000000000000000000001000000,
    }

    if !expectedMask.equals(actualMask) {
        t.Errorf("Expected %#v to equal %#v\n", actualMask, expectedMask)
    }
}

func TestApplyMask(t *testing.T) {
    value := 42
    mask := parseMask("000000000000000000000000000000X1001X")

    actualValue := mask.apply(value)
    expectedValue := []int{26, 27, 58, 59}

    equals := func(a, b []int) bool {
        if len(a) != len(b) { return false }

        acopy := make([]int, len(a))
        bcopy := make([]int, len(b))

        copy(acopy, a)
        copy(bcopy, b)

        sort.Ints(acopy)
        sort.Ints(bcopy)

        for i, v := range acopy {
            if v != bcopy[i] { return false }
        }

        return true
    }

    if !equals(expectedValue, actualValue) {
        t.Errorf("Expected %#v to equal %#v\n", actualValue, expectedValue)
    }
}

func TestParseLineMask(t *testing.T) {
    input := "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"

    actualInstr, err := parseLine(input)
    expectedInstr := &setMask{msk: parseMask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X")}

    if err != nil {
        t.Errorf("Failed with error %s", err)
    }

    if !expectedInstr.equals(actualInstr) {
        t.Errorf("Expected %#v to equal %#v", actualInstr, expectedInstr)
    }
}

func TestParseLineValue(t *testing.T) {
    input := "mem[7] = 101"

    actualInstr, err := parseLine(input)
    expectedInstr := &setValue{addr: 7, value: 101}

    if err != nil {
        t.Errorf("Failed with error %s", err)
    }

    if !expectedInstr.equals(actualInstr) {
        t.Errorf("Expected %#v to equal %#v", actualInstr, expectedInstr)
    }
}
