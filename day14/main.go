package main

import(
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "regexp"
    "strings"
    "strconv"
)

type mask struct {
    filter int
    value int
}

func (m *mask) equals(other *mask) bool {
    return m.filter == other.filter && m.value == other.value
}

func formatBin(vals []int) string {
    items := []string{}

    for _, v := range vals {
        items = append(items, fmt.Sprintf("%036b", v))
    }

    return strings.Join(items, " ")
}

func (m *mask) apply(value int) []int {
    variations := []int{0}

    for i := 35; i >= 0; i-- {
        filterBit := (m.filter >> i) & 1

        if filterBit == 0 {
            maskValue := (m.value >> i) & 1

            for j, _ := range variations {
                variations[j] <<= 1

                if maskValue == 1 {
                    variations[j] |= maskValue
                    continue
                }

                variations[j] |= (value >> i) & 1
            }

            continue
        }

        for j, _ := range variations {
            variations[j] <<= 1
        }

        newVariations := []int{}
        for _, v := range variations {
            newVariations = append(newVariations, v | 1)
        }

        variations = append(variations, newVariations...)
    }

    return variations
}

func parseMask(line string) *mask {
    m := mask{}

    for _, i := range []rune(line) {
        filterBit := 0

        if i == 'X' {
            filterBit = 1
        } else {
            filterBit = 0
        }

        m.filter <<= 1
        m.filter |= filterBit

        valueBit := 0

        if i == '1' {
            valueBit = 1
        } else {
            valueBit = 0
        }

        m.value <<= 1
        m.value |= valueBit
    }
    return &m
}

type state struct {
    mem map[int]int
    msk *mask
}

type instruction interface {
    equals(instruction) bool
    exec(s *state)
}

type setValue struct {
    addr int
    value int
}

func (i *setValue) equals(other instruction) bool {
    o, ok := other.(*setValue)

    if !ok { return false }


    if i.addr != o.addr { return false }
    
    return i.value == o.value
}

func (i *setValue) exec(s *state) {
    for _, v := range s.msk.apply(i.addr) {
        s.mem[v] = i.value
    }
}

type setMask struct {
    msk *mask
}

func (i *setMask) equals(other instruction) bool {
    o, ok := other.(*setMask)

    if !ok { return false }

    return i.msk.equals(o.msk)
}

func (i *setMask) exec(s *state) {
    s.msk = i.msk
}

func load(filename string) ([]instruction, error) {
    file, err := os.Open(filename)

    if err != nil {
        return nil, err
    }

    scanner := bufio.NewScanner(file)
    instructions := []instruction{}

    for scanner.Scan() {
        instr, err := parseLine(scanner.Text())

        if err != nil {
            return nil, err
        }

        instructions = append(instructions, instr)
    }

    if err = scanner.Err(); err != nil {
        return nil, err
    }

    return instructions, nil
}

var maskRegexp, _ = regexp.Compile("^mask = ([X01]+)$")
var valueRegexp, _ = regexp.Compile("^mem\\[(\\d+)\\] = (\\d+)$")
func parseLine(line string) (instruction, error) {
    groups := maskRegexp.FindStringSubmatch(line)

    if groups != nil {
        return &setMask{msk: parseMask(groups[1])}, nil
    }

    groups = valueRegexp.FindStringSubmatch(line)

    if groups != nil {
        addr, err := strconv.ParseInt(groups[1], 10, 64)

        if err != nil {
            return nil, err
        }

        value, err := strconv.ParseInt(groups[2], 10, 64)

        if err != nil {
            return nil, err
        }

        return &setValue{addr: int(addr), value: int(value)}, nil
    }

    return nil, fmt.Errorf("Cannot parse line '%s'", line)
}

func run(mem map[int]int, instructions []instruction) {
    s := &state{mem: mem}

    for _, instr := range instructions {
        instr.exec(s)
    }
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
        return
    }

    instructions, err := load(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    mem := map[int]int{}

    run(mem, instructions)

    sum := 0

    for _, v := range mem { sum += v }

    fmt.Println(sum)
}
