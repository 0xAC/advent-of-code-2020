package main

import "testing"

func createState(numbers []int, value int) *state {
	head := &node{value: numbers[0]}
	current := head

	tail := head
	for _, n := range numbers[1:] {
		tail = tail.append(n)

		if n == value {
			current = tail
		}
	}

	tail.next = head

	return &state{head: head, current: current}
}

func TestStart(t *testing.T) {
	ga, err := newGame("389125467", 0)

	if err != nil {
		t.Error(err)
		return
	}

	expSt := createState(
		[]int{3, 8, 9, 1, 2, 5, 4, 6, 7},
		3,
	)

	if !expSt.equals(ga.st) {
		t.Errorf("-- move 1 --\nExpected:\n\n%s\n\nbut got:\n\n%s", expSt, ga.st)
	}

	moves := []*state{
		createState(
			[]int{3, 2, 8, 9, 1, 5, 4, 6, 7},
			2,
		),
		createState(
			[]int{3, 2, 5, 4, 6, 7, 8, 9, 1},
			5,
		),
		createState(
			[]int{7, 2, 5, 8, 9, 1, 3, 4, 6},
			8,
		),
		createState(
			[]int{3, 2, 5, 8, 4, 6, 7, 9, 1},
			4,
		),
		createState(
			[]int{9, 2, 5, 8, 4, 1, 3, 6, 7},
			1,
		),
		createState(
			[]int{7, 2, 5, 8, 4, 1, 9, 3, 6},
			9,
		),
		createState(
			[]int{8, 3, 6, 7, 4, 1, 9, 2, 5},
			2,
		),
		createState(
			[]int{7, 4, 1, 5, 8, 3, 9, 2, 6},
			6,
		),
		createState(
			[]int{5, 7, 4, 1, 8, 3, 9, 2, 6},
			5,
		),
		createState(
			[]int{5, 8, 3, 7, 4, 1, 9, 2, 6},
			8,
		),
	}

	for i, move := range moves {
		ga.move()
		ga.norm(i + 1)

		if !move.equals(ga.st) {
			t.Errorf("-- move %d --\nExpected:\n\n%s\n\nbut got:\n\n%s", i+2, move, ga.st)
		}
	}
}
