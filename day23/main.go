package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
)

type game struct {
	st     *state
	pocket *cache
}

func (g *game) findDest(current int) *node {
	dest := current - 1

	if dest < g.pocket.min {
		dest = g.pocket.max
	}

	head := g.st.current
	for i := 0; i < 3; i++ {
		head = head.next

		if head.value == dest {
			return g.findDest(dest)
		}
	}

	return g.pocket.nodes[dest-1]
}

func (g *game) move() {
	current := g.st.current
	dest := g.findDest(current.value)

	fst := current.next
	lst := fst.next.next

	current.next = lst.next
	dest.next, lst.next = fst, dest.next

	g.st.current = current.next
}

func (g *game) norm(i int) {
	// This make comparison with expected results on AoC webside easy
	head := g.st.head

	for {
		n := head
		for j := 0; j < i; j++ {
			n = n.next
		}

		if n == g.st.current {
			break
		}

		head = head.next
	}

	g.st.head = head
}

func (g *game) result1() int {
	head := g.st.current
	result := 0

	for {
		if head.value != 1 {
			head = head.next
			continue
		}

		head = head.next

		for {
			if head.value == 1 {
				break
			}

			result = result*10 + head.value
			head = head.next
		}

		break
	}

	return result
}

func parseNumbers(input string) ([]int, error) {
	numbers := []int{}

	for _, n := range strings.Split(input, "") {
		number, err := strconv.ParseInt(n, 10, 64)

		if err != nil {
			return nil, err
		}

		numbers = append(numbers, int(number))
	}

	return numbers, nil
}

type cache struct {
	min, max int
	nodes    []*node
}

func (c *cache) update(n *node) {
	value := n.value
	c.nodes[value-1] = n

	if value < c.min {
		c.min = value
	}

	if value > c.max {
		c.max = value
	}
}

func newCache(n *node, size int) *cache {
	value := n.value
	nodes := make([]*node, size)
	nodes[value-1] = n

	return &cache{min: value, max: value, nodes: nodes}
}

func newGame(input string, fill int) (*game, error) {
	numbers, err := parseNumbers(input)

	if err != nil {
		return nil, err
	}

	size := len(numbers)

	if fill > size {
		size = fill
	}

	head := &node{value: numbers[0]}
	pocket := newCache(head, size)
	tail := head

	for _, n := range numbers[1:] {
		tail = tail.append(n)
		pocket.update(tail)
	}

	for i := pocket.max + 1; i <= fill; i++ {
		tail = tail.append(i)
		pocket.update(tail)
	}

	tail.next = head

	ga := &game{st: &state{head: head, current: head}, pocket: pocket}
	return ga, nil
}

func (g *game) loop(count int) {
	for i := 0; i < count; i++ {
		g.move()
	}
}

func (g *game) result2() int {
	head := g.st.current

	for {
		if head.value == 1 {
			break
		}

		head = head.next
	}

	next := head.next
	a := next.value
	b := next.next.value

	return a * b
}

func main() {
	ga, err := newGame("315679824", 0)

	if err != nil {
		log.Fatal(err)
	}

	ga.loop(100)

	fmt.Printf("part 1: %d\n", ga.result1())

	ga, err = newGame("315679824", 1000000)

	if err != nil {
		log.Fatal(err)
	}

	ga.loop(10000000)

	fmt.Printf("part 2: %d\n", ga.result2())
}
