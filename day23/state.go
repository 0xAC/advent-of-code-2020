package main

import (
	"fmt"
	"strings"
)

type node struct {
	next  *node
	value int
}

func (n *node) append(value int) *node {
	if n.next == nil {
		n.next = &node{value: value}
		return n.next
	}

	return n.next.append(value)
}

type state struct {
	head    *node
	current *node
}

func (s *state) String() string {
	cups := []string{}

	head := s.head
	tail := s.head
	for {
		format := "%d"

		if head.value == s.current.value {
			format = "(" + format + ")"
		}

		cups = append(cups, fmt.Sprintf(format, head.value))
		head = head.next

		if head == tail {
			break
		}
	}

	return fmt.Sprintf("cups: %s", strings.Join(cups, " "))
}

func (s *state) equals(o *state) bool {
	if s.current.value != o.current.value {
		return false
	}

	head1 := s.head
	head2 := o.head

	fmt.Println("EQ")
	for {
		if head1.next == s.head {
			if head2.next == o.head {
				break
			}

			return false
		}

		if head2.next == o.head {
			return false
		}

		if head1.value != head2.value {
			return false
		}

		head1 = head1.next
		head2 = head2.next
	}
	fmt.Println("EQ2")

	return true
}
