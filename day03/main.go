package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
)

type grid struct {
    height int
    runes []rune
    width int
}

func read(filename string) (grid, error) {
    file, err := os.Open(filename)
    g := grid{}

    if err != nil {
        return g, err
    }

    defer file.Close()

    scanner := bufio.NewScanner(file)

    for scanner.Scan() {
        line := []rune(scanner.Text())
        g.width = len(line)

        if err != nil {
            return g, err
        }

        g.runes = append(g.runes, line...)
    }

    if err := scanner.Err(); err != nil {
        return g, err
    }

    g.height = len(g.runes) / g.width
    return g, nil
}

func count(g grid, s slope) int {
    c := 0
    for i, j := 0, 0; i < g.height; i, j = i + s.y, (j + s.x) % g.width {
        if (g.runes[g.width * i + j] == '#') {
            c++
        }
    }

    return c
}

type slope struct {
    x int
    y int
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input]\n", filepath.Base(os.Args[0]))
        return
    }

    g, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    sum := 1
    slopes := []slope { { 1, 1 }, { 3, 1 }, { 5, 1 }, { 7, 1 }, { 1, 2 } }

    for _, s := range slopes {
        sum *= count(g, s)
    }

    fmt.Println(sum)
}
