package main

import (
    "fmt"
    "regexp"
    "strings"
)

type altRule struct {
    rules []*seqRule
}

func (r *altRule) match(rules map[int]rule, text string) ([]string, bool) {
    matches := []string{}
    for _, ru := range r.rules {
        match, ok := ru.match(rules, text)

        if !ok { continue }

        matches = append(matches, match...)
    }

    return matches, true
}

func (r *altRule) String() string {
    parts := []string{}

    for _, ru := range r.rules {
        parts = append(parts, ru.String())
    }

    return strings.Join(parts, " ")
}

func (r *altRule) equals(otherRu rule) bool {
    other, ok := otherRu.(*altRule)

    if !ok { return false }

    if len(r.rules) != len(other.rules) {
        return false
    }

    for i, ru := range r.rules {
        if !ru.equals(other.rules[i]) { return false }
    }

    return true
}

func newAltRuleMatcher() ruleMatcher {
    matcher := ruleMatcher{}

    exp, err := regexp.Compile("^([^|]+)( \\| ([^|]+))+$")

    if err != nil { panic(err) }

    matcher.exp = exp
    matcher.create = func(parser *ruleParser, groups []string) (rule, error) {
        rules := []*seqRule{}

        for _, ruleString := range strings.Split(groups[0], " | ") {
            ru, err := parser.parseRule(ruleString)

            if err != nil { return nil, err }

            seqRu, ok := ru.(*seqRule)

            if !ok {
                return nil, fmt.Errorf("AltRule can contain only seqRules")
            }

            rules = append(rules, seqRu)
        }

        return &altRule{rules}, nil
    }

    return matcher
}
