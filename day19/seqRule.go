package main

import (
    "fmt"
    "regexp"
    "strings"
)

type seqRule struct {
    ruleIds []int
}

func do(rules map[int]rule, text string, ruIds []int) ([]string, bool) {
    parts, ok := rules[ruIds[0]].match(rules, text)

    if !ok { return nil, false }
    if len(ruIds) == 1 { return parts, true }

    matches := []string{}

    for _, p := range parts {
        subparts, ok := do(rules, string(text[len(p):]), ruIds[1:])

        if !ok { continue }

        for _, s := range subparts {
            matches = append(matches, p + s)
        }
    }

    if len(matches) == 0 {
        return nil, false
    }

    return matches, true
}

func (r *seqRule) match(rules map[int]rule, text string) ([]string, bool) {
    return do(rules, text, r.ruleIds)
}

func (r *seqRule) String() string {
    parts := []string{}

    for _, id := range r.ruleIds {
        parts = append(parts, fmt.Sprintf("%d", id))
    }

    return strings.Join(parts, " ")
}

func (r *seqRule) equals(otherRu rule) bool {
    other, ok := otherRu.(*seqRule)

    if !ok { return false }

    if len(r.ruleIds) != len(other.ruleIds) {
        return false
    }

    for i, id := range r.ruleIds {
        if id != other.ruleIds[i] { return false }
    }

    return true
}

func newSeqRuleMatcher() ruleMatcher {
    matcher := ruleMatcher{}

    exp, err := regexp.Compile("^(\\d+)( \\d+)*$")

    if err != nil { panic(err) }

    matcher.exp = exp
    matcher.create = func(_ *ruleParser, groups []string) (rule, error) {
        ids := []int{}

        for _, idString := range strings.Split(groups[0], " ") {
            id, err := parseInt(idString)

            if err != nil { return nil, err }

            ids = append(ids, id)
        }

        return &seqRule{ids}, nil
    }

    return matcher
}

