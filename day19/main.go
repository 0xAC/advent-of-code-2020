package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "strconv"
    "strings"
)

type rule interface {
    equals(rule) bool
    match(map[int]rule,string)([]string,bool)
    String() string
}

func parseInt(input string) (int, error) {
    value, err := strconv.ParseInt(input, 10, 64)

    if err != nil {
        return 0, err
    }

    return int(value), nil
}

func readText(filename string) (string, error) {
    file, err := os.Open(filename)

    if err != nil { return "", err }

    scanner := bufio.NewScanner(file)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err = scanner.Err(); err != nil {
        return "", err
    }

    return strings.Join(lines, "\n"), nil
}

type data struct {
    rules map[int]rule
    lines []string
}

func (d *data) count() int {
    count := 0
    rule := d.rules[0]

    for _, line := range d.lines {
        matches, ok := rule.match(d.rules, line)

        if !ok { continue }

        for _, match := range matches {
            if match == line {
                count++
                break
            }
        }

    }

    return count
}

func readData(filename string) (*data, error) {
    d := data{}

    text, err := readText(filename)

    if err != nil { return nil, err }

    parts := strings.Split(text, "\n\n")

    if len(parts) != 2 {
        return nil, fmt.Errorf("Files has to consist of 2 sections separeted by a blank line")
    }

    parser := newParser()
    d.rules, err = parser.parseAll(strings.Split(parts[0], "\n"))

    if err != nil { return nil, err }

    d.lines = strings.Split(parts[1], "\n")

    return &d, nil
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file}\n", filepath.Base(os.Args[0]));
        return
    }

    dat, err := readData(os.Args[1])

    if err != nil { log.Fatal(err) }

    fmt.Println("part 1:", dat.count())
    parser := newParser()

    dat.rules[8], err = parser.parseRule("42 | 42 8")
    if err != nil { log.Fatal(err) }

    dat.rules[11], err = parser.parseRule("42 31 | 42 11 31")
    if err != nil { log.Fatal(err) }

    fmt.Println(len(dat.lines))
    fmt.Println("part 2:", dat.count())
}

