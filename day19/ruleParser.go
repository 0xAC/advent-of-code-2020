package main

import (
    "fmt"
    "regexp"
)

var ruleRegexp, _ = regexp.Compile("(\\d+): (.*)")

type ruleMatcher struct {
    exp *regexp.Regexp
    create func(*ruleParser, []string)(rule, error)
}

type ruleParser struct {
    matchers []ruleMatcher
}

func (p *ruleParser) parseRule(text string) (rule, error) {
    for _, m := range p.matchers {
        if groups := m.exp.FindStringSubmatch(text); groups != nil {
            ru, err := m.create(p, groups)

            if err != nil { return nil, err }

            return ru, nil
        }
    }

    return nil, fmt.Errorf("No rule matcher for rule '%s'", text)
}

func (p *ruleParser) parseLine(line string) (int, rule, error) {
    groups := ruleRegexp.FindStringSubmatch(line)

    if groups == nil {
        return 0, nil, fmt.Errorf("Unexpected line: %s", line)
    }

    id, err := parseInt(groups[1])

    if err != nil {
        return id, nil, err
    }

    ru, err := p.parseRule(groups[2])

    if err != nil {
        return id, nil, err
    }

    return id, ru, nil
}

func (p *ruleParser) parseAll(lines []string) (map[int]rule, error) {
    dict := map[int]rule{}

    for _, line := range lines {
        id, ru, err := p.parseLine(line)

        if err != nil {
            return nil, err
        }

        dict[id] = ru
    }

    return dict, nil
}

func newParser() *ruleParser {
    matchers := []ruleMatcher{
        newAltRuleMatcher(),
        newCharRuleMatcher(),
        newSeqRuleMatcher(),
    }

    return &ruleParser{matchers}
}
