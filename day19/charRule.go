package main

import (
    "fmt"
    "regexp"
)

type charRule struct {
    char rune
}

func (r *charRule) match(_ map[int]rule, text string) ([]string, bool) {
    if len(text) < 1 { return nil, false }

    chars := []rune(text)

    if chars[0] != r.char { return nil, false }

    return []string{string([]rune{r.char})}, true
}

func (r *charRule) String() string {
    return fmt.Sprintf("\"%c\"", r.char)
}

func (r *charRule) equals(otherRu rule) bool {
    other, ok := otherRu.(*charRule)

    if !ok { return false }

    return r.char == other.char
}

func newCharRuleMatcher() ruleMatcher {
    matcher := ruleMatcher{}

    exp, err := regexp.Compile("\"(\\w)\"")

    if err != nil { panic(err) }

    matcher.exp = exp
    matcher.create = func(_ *ruleParser, groups []string) (rule, error) {
        chars := []rune(groups[1])
        return &charRule{chars[0]}, nil
    }

    return matcher
}

