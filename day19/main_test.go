package main

import (
    "testing"
)

func testParseRule(t *testing.T, line string, expId int, expRu rule) {
    parser := newParser()
    id, ru, err := parser.parseLine(line)

    if err != nil {
        t.Errorf("Error occured: '%s'", err)
        return
    }

    if id != expId {
        t.Errorf("id '%d' does not equal '%d'", id, expId)
        return
    }

    if !expRu.equals(ru) {
        t.Errorf("Expected '%s' but got '%s'", expRu, ru)
    }
}

func TestParseSimpleRule0a(t *testing.T) {
    line := "0: \"a\""

    testParseRule(t, line, 0, &charRule{'a'})
}

func TestParseSimpleRule1a(t *testing.T) {
    line := "1: \"a\""

    testParseRule(t, line, 1, &charRule{'a'})
}

func TestParseSimpleRule0b(t *testing.T) {
    line := "1: \"b\""

    testParseRule(t, line, 1, &charRule{'b'})
}

func TestParseSeqRule(t *testing.T) {
    line := "0: 1 2"

    testParseRule(t, line, 0, &seqRule{[]int{1, 2}})
}

func TestParseSeqRuleSingle(t *testing.T) {
    line := "0: 1"

    testParseRule(t, line, 0, &seqRule{[]int{1}})
}

func TestParseAltRule(t *testing.T) {
    line := "0: 1 2 | 4 2"
    subRules := []*seqRule{&seqRule{[]int{1, 2}}, &seqRule{[]int{4, 2}}}

    testParseRule(t, line, 0, &altRule{subRules})
}

func testMatchRule(t *testing.T, ruleLines []string, value string) {
    parser := newParser()
    dict, err := parser.parseAll(ruleLines)

    if err != nil {
        t.Errorf("Error occured: '%s'", err)
        return
    }

    _, ok := dict[0].match(dict, value)

    if !ok {
        t.Errorf("Rule '%s' does not match '%s'", dict[0], value)
        return
    }
}

func TestMatchCharRule(t *testing.T) {
    lines := []string{
        "0: \"a\"",
    }

    testMatchRule(t, lines, "a")
}

func TestMatchSeqRule(t *testing.T) {
    lines := []string{
        "0: 1 2 3",
        "1: \"a\"",
        "2: \"b\"",
        "3: \"c\"",
    }

    testMatchRule(t, lines, "abc")
}

func TestMatchAltRule(t *testing.T) {
    lines := []string{
        "0: 1 2 | 3 4",
        "1: \"a\"",
        "2: \"b\"",
        "3: \"c\"",
        "4: \"d\"",
    }

    testMatchRule(t, lines, "ab")
}

func TestMatchMultiRule(t *testing.T) {
    lines := []string{
        "0: 4 1 5",
        "1: 2 3 | 3 2",
        "2: 4 4 | 5 5",
        "3: 4 5 | 5 4",
        "4: \"a\"",
        "5: \"b\"", }

    testMatchRule(t, lines, "aaabab")
}

func TestCount(t *testing.T) {
    ruleLines := []string{
        "0: 8 11",
        "1: \"a\"",
        "2: 1 24 | 14 4",
        "3: 5 14 | 16 1",
        "4: 1 1",
        "5: 1 14 | 15 1",
        "6: 14 14 | 1 14",
        "7: 14 5 | 1 21",
        "9: 14 27 | 1 26",
        "10: 23 14 | 28 1",
        "12: 24 14 | 19 1",
        "13: 14 3 | 1 12",
        "14: \"b\"",
        "15: 1 | 14",
        "16: 15 1 | 14 14",
        "17: 14 2 | 1 7",
        "18: 15 15",
        "19: 14 1 | 14 14",
        "20: 14 14 | 1 15",
        "21: 14 1 | 1 14",
        "22: 14 14",
        "23: 25 1 | 22 14",
        "24: 14 1",
        "25: 1 1 | 1 14",
        "26: 14 22 | 1 20",
        "27: 1 6 | 14 18",
        "28: 16 1",
        "31: 14 17 | 1 13",
        "42: 9 14 | 10 1",
        "8: 42 | 42 8",
        "11: 42 31 | 42 11 31",
    }

    parser := newParser()
    rules, err := parser.parseAll(ruleLines)

    if err != nil {
        t.Errorf("Error occured: '%s'", err)
        return
    }

    lines := []string{
        "abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa",
        "bbabbbbaabaabba",
        "babbbbaabbbbbabbbbbbaabaaabaaa",
        "aaabbbbbbaaaabaababaabababbabaaabbababababaaa",
        "bbbbbbbaaaabbbbaaabbabaaa",
        "bbbababbbbaaaaaaaabbababaaababaabab",
        "ababaaaaaabaaab",
        "ababaaaaabbbaba",
        "baabbaaaabbaaaababbaababb",
        "abbbbabbbbaaaababbbbbbaaaababb",
        "aaaaabbaabaaaaababaa",
        "aaaabbaaaabbaaa",
        "aaaabbaabbaaaaaaabbbabbbaaabbaabaaa",
        "babaaabbbaaabaababbaabababaaab",
        "aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba",
    }

    rules, err = parser.parseAll(ruleLines)

    if err != nil { t.Error(err) }

    d := data{rules, lines}
    expectedCount := 12
    count := d.count()

    if count != expectedCount {
        t.Errorf("Expected %d but got %d\n", expectedCount, count)
    }
}
