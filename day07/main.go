package main

import(
   "bufio"
   "fmt"
   "log"
   "os"
   "path/filepath"
   "regexp"
   "strconv"
   "strings"
)

// light red bags contain 1 bright white bag, 2 muted yellow bags.
// dark orange bags contain 3 bright white bags, 4 muted yellow bags.
// bright white bags contain 1 shiny gold bag.
// muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
// shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
// dark olive bags contain 3 faded blue bags, 4 dotted black bags.
// vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
// faded blue bags contain no other bags.
// dotted black bags contain no other bags.
type contain struct {
    bags []bag
    name string
}

type bag struct {
    name string
    count int
}

func(r contain) String() string {
    value := r.name + "["

    for i, c := range r.bags {
        value += c.String()

        if i < len(r.bags) - 1 {
            value += ", "
        }
    }

    return value + "]"
}

func (b bag) String() string {
    return fmt.Sprintf("%d %s", b.count, b.name)
}

var ruleRegexp, _ = regexp.Compile("^(\\w+ \\w+) bags contain ([^\\.]+).$")
var insideRegexp, _ = regexp.Compile("^(\\d+) (\\w+ \\w+) bag(s?)$")

func parseContent(line string) ([]bag, error) {
    if line == "no other bags" {
        return []bag{}, nil
    }

    bags := []bag{}

    for _, it := range strings.Split(line, ", ") {
        groups := insideRegexp.FindStringSubmatch(it)

        if groups == nil {
            return bags, fmt.Errorf("Cannot parse sub-rule '%s'", line)
        }

        val, err := strconv.ParseInt(groups[1], 10, 32)

        if err != nil {
            return bags, err
        }

        bags = append(bags, bag{groups[2], int(val)})
    }

    return bags, nil
}

func parse(line string) (contain, error) {
    r := contain{}
    groups := ruleRegexp.FindStringSubmatch(line)

    if groups == nil {
        return r, fmt.Errorf("Cannot parse line '%s'", line)
    }

    r.name = groups[1]
    bags, err := parseContent(groups[2])

    if err != nil {
        return r, err
    }

    r.bags = bags

    return r, nil
}

func read(filename string) ([]contain, error) {
    file, err := os.Open(filename)
    rules := []contain{}

    if err != nil {
        return rules, err
    }

    scanner := bufio.NewScanner(file)

    for scanner.Scan() {
        r, err := parse(scanner.Text())

        if err != nil {
            return rules, err
        }

        rules = append(rules, r)
    }

    if err = scanner.Err(); err != nil {
        return rules, err
    }

    return rules, nil
}

func index(contains []contain) map[string][]bag {
    m := map[string][]bag{}

    for _, c := range contains {
        m[c.name] = c.bags
    }

    return m
}

func invert(in map[string][]bag) map[string][]bag {
    out := map[string][]bag{}

    for name, bags := range in {
        for _, b := range bags {
            bs, ok := out[b.name]

            if !ok {
                bs = []bag{}
            }

            out[b.name] = append(bs, bag{name, 1})
        }
    }

    return out
}

func find(in map[string][]bag, name string) []string {
    bags, ok := in[name]

    if !ok {
        return []string{}
    }

    results := []string{}

    for _, b := range bags {
        results = append(results, b.name)
        results = append(results, find(in, b.name)...)
    }

    m := map[string]bool{}
    out := []string{}

    for _, r := range results {
        m[r] = true
    }

    for k, _ := range m {
        out = append(out, k)
    }

    return out
}

func count(in map[string][]bag, name string) int {
    sum := 0

    for _, b := range in[name] {
        sum += b.count
        sum += count(in, b.name) * b.count
    }

    return sum
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
        return
    }

    contains, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    fmt.Println(count(index(contains), "shiny gold"))
}
