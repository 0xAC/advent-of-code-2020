package main

import(
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
)

type pos [4]int // change to 3 to work in 3D space

func read(filename string) (space, error) {
    file, err := os.Open(filename)

    if err != nil {
        return nil, err
    }

    scanner := bufio.NewScanner(file)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    cubes := space{}

    for y := len(lines) - 1; y >= 0; y-- {
        p := pos{}
        p[1] = y

        for x, chr := range lines[y] {
            if chr == '#' {
                p[0] = x

                cubes[p] = true
            }
        }
    }

    if err = scanner.Err(); err != nil {
        return nil, err
    }
    
    return cubes, nil
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
        return
    }

    cubes, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    for i := 0; i < 6; i++ { cubes.cycle() }

    fmt.Println(cubes.count())
}

type space map[pos]bool

type size struct {
    min, max pos
}

func (s space) count() int {
    count := 0

    for range s { count++ }

    return count
}

func (s space) size() size {
    sz := size{}

    for coords, _ := range s {
        for i, coord := range coords {
            if coord < sz.min[i] { sz.min[i] = coord }
            if coord > sz.max[i] { sz.max[i] = coord }
        }
    }

    return sz
}

func (s space) neighbours(i int, subject, current pos) int {
    if i == len(subject) {
        if s[current] && current != subject {
            return 1
        }

        return 0
    }

    count := 0

    for j := subject[i] - 1; j <= subject[i] + 1; j++ {
        current[i] = j

        count += s.neighbours(i + 1, subject, current)
    }

    return count
}

type operation interface {
    apply(s space)
}

type activate struct { position pos }

func (a activate) apply(s space) {
    s[a.position] = true
}

type deactivate struct { position pos }

func (d deactivate) apply(s space) {
    delete(s, d.position)
}

func (s space) iterate(i int, sz *size, subject pos) []operation {
    operations := []operation{}

    if i < len(subject) {
        for j := sz.min[i] - 1; j <= sz.max[i] + 1; j++ {
            subject[i] = j

            operations = append(operations, s.iterate(i + 1, sz, subject)...)
        }

        return operations
    }

    _, active := s[subject]
    count := s.neighbours(0, subject, pos{})

    if active {
        if count != 2 && count != 3 {
            return []operation{deactivate{subject}}
        }

        return operations
    }

    if count == 3 {
        return []operation{activate{subject}}
    }

    return operations
}

func (s space) cycle() {
    sz := s.size()

    for _, op := range s.iterate(0, &sz, pos{}) {
        op.apply(s)
    }
}
