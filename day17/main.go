package main

import(
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "strings"
)

type line map[int]rune
type plane map[int]line
type cube map[int]plane
type hyper map[int]cube

func (l line) print(min, max int) string {
    chars := []rune{}

    for i := min; i <= max; i++ {
        _, ok := l[i]

        if !ok {
            chars = append(chars, '.')
            continue
        }

        chars = append(chars, '#')
    }

    return string(chars)
}

func (l line) size() (int, int) {
    set := false
    min, max := 0, 0

    for i, _ := range l {
        if !set {
            min = i
            max = i
            set = true
            continue
        }

        if i < min { min = i }
        if i > max { max = i }
    }

    return min, max
}

func (l line) get(x int) bool {
    _, ok := l[x]

    if !ok { return false }

    return true
}

func (l line) set(x int) {
    l[x] = '#'
}

func (l line) unset(x int) {
    delete(l, x)
}

func (l line) clone() line {
    clone := line{}

    for x, chr := range l {
        clone[x] = chr
    }

    return clone
}

func emptyLine(size int) string {
    chars := []rune{}

    for i := 0; i < size; i++ {
        chars = append(chars, '.')
    }

    return string(chars)
}

func (p plane) print(minx, maxx, miny, maxy int) string {
    lines := []string{}

    for y := maxy; y >= miny; y-- {
        ln, ok := p[y]

        if !ok {
            lines = append(lines, emptyLine(maxx - minx + 1))
            continue
        }

        lines = append(lines, ln.print(minx, maxx))
    }

    return strings.Join(lines, "\n")
}

func (p plane) size() (int, int, int, int) {
    tminx, tmaxx, miny, maxy := 0, 0, 0, 0
    set := false

    for y, v := range p {
        if !set {
            miny = y
            maxy = y

            tminx, tmaxx = v.size()

            set = true
            continue
        }

        if y < miny { miny = y }
        if y > maxy { maxy = y }

        minx, maxx := v.size()

        if minx < tminx { tminx = minx }
        if maxx > tmaxx { tmaxx = maxx }
    }

    return tminx, tmaxx, miny, maxy
}

func (p plane) get(x, y int) bool {
    ln, ok := p[y]

    if !ok { return false }

    return ln.get(x)
}

func (p plane) set(x, y int) {
    ln, ok := p[y]

    if !ok {
        ln = line{}
    }

    ln.set(x)
    p[y] = ln
}

func (p plane) unset(x, y int) {
    ln, ok := p[y]

    if !ok { return }

    ln.unset(x)

    if len(ln) == 0 {
        delete(p, y)
    }
}

func (p plane) clone() plane {
    clone := plane{}

    for y, ln:= range p {
        clone[y] = ln.clone()
    }

    return clone
}

func (c cube) size() (int, int, int, int, int, int) {
    tminx, tmaxx, tminy, tmaxy, minz, maxz := 0, 0, 0, 0, 0, 0
    set := false

    for z, pl := range c {
        if !set {
            tminx, tmaxx, tminy, tmaxy = pl.size()
            minz = z
            maxz = z
            set = true
            continue
        }

        if z < minz { minz = z }
        if z > maxz { maxz = z }

        minx, maxx, miny, maxy := pl.size()

        if miny < tminy { tminy = miny }
        if maxy > tmaxy { tmaxy = maxy }
        if minx < tminx { tminx = minx }
        if maxx > tmaxx { tmaxx = maxx }
    }

    return tminx, tmaxx, tminy, tmaxy, minz, maxz
}

func (c cube) print(minx, maxx, miny, maxy, minz, maxz, w int) string {
    text := []string{}

    for z := minz; z <= maxz; z++ {
        pl, ok := c[z]

        if !ok { continue }

        text = append(text, fmt.Sprintf("\nz=%d, w=%d", z, w))
        text = append(text, pl.print(minx, maxx, miny, maxy))
    }

    return strings.Join(text, "\n")
}

func (c cube) get(x, y, z int) bool {
    pl, ok := c[z]

    if !ok { return false }

    return pl.get(x, y)
}

func (c cube) set(x, y, z int) {
    pl, ok := c[z]

    if !ok {
        pl = plane{}
    }

    pl.set(x, y)
    c[z] = pl
}

func (c cube) unset(x, y, z int) {
    pl, ok := c[z]

    if !ok { return }

    pl.unset(x, y)

    if len(pl) == 0 {
        delete(c, z)
    }
}

func (c cube) clone() cube {
    clone := cube{}

    for z, pl := range c {
        clone[z] = pl.clone()
    }

    return clone
}

func (h hyper) size() (int, int, int, int, int, int, int, int) {
    tminx, tmaxx, tminy, tmaxy, tminz, tmaxz, minw, maxw := 0, 0, 0, 0, 0, 0, 0, 0
    set := false

    for w, cu := range h {
        if !set {
            tminx, tmaxx, tminy, tmaxy, tminz, tmaxz = cu.size()
            minw = w
            maxw = w
            set = true
            continue
        }

        if w < minw { minw = w }
        if w > maxw { maxw = w }

        minx, maxx, miny, maxy, minz, maxz := cu.size()

        if miny < tminy { tminy = miny }
        if maxy > tmaxy { tmaxy = maxy }
        if minx < tminx { tminx = minx }
        if maxx > tmaxx { tmaxx = maxx }
        if minz < tminz { tminz = minz }
        if maxz > tmaxz { tmaxz = maxz }
    }

    return tminx, tmaxx, tminy, tmaxy, tminz, tmaxz, minw, maxw
}

func (h hyper) print() string {
    minx, maxx, miny, maxy, minz, maxz, minw, maxw := h.size()
    text := []string{}

    for w := minw; w <= maxw; w++ {
        cu, ok := h[w]

        if !ok { continue }

        text = append(text, cu.print(minx, maxx, miny, maxy, minz, maxz, w))
    }

    return strings.Join(text, "\n")
}

func (h hyper) get(x, y, z, w int) bool {
    cu, ok := h[w]

    if !ok { return false }

    return cu.get(x, y, z)
}

func (h hyper) set(x, y, z, w int) {
    cu, ok := h[w]

    if !ok {
        cu = cube{}
    }

    cu.set(x, y, z)
    h[w] = cu
}

func (h hyper) unset(x, y, z, w int) {
    cu, ok := h[w]

    if !ok { return }

    cu.unset(x, y, z)

    if len(cu) == 0 {
        delete(h, w)
    }
}

func (h hyper) clone() hyper {
    clone := hyper{}

    for w, cu := range h {
        clone[w] = cu.clone()
    }

    return clone
}

func (h hyper) cycle() {
    clone := h.clone()
    minx, maxx, miny, maxy, minz, maxz, minw, maxw := h.size()

    for w := minw - 1; w <= maxw + 1; w++ {
        for z := minz - 1; z <= maxz + 1; z++ {
            for y := miny - 1; y <= maxy + 1; y++ {
                for x := minx - 1; x <= maxx + 1; x++ {
                    activeCount := 0

                    outer:
                    for dx := -1; dx <= 1; dx++ {
                        for dy := -1; dy <= 1; dy++ {
                            for dz := -1; dz <= 1; dz++ {
                                for dw := -1; dw <= 1; dw++ {
                                    if dx == 0 && dy == 0 && dz == 0 && dw == 0 { continue }

                                    px, py, pz, pw := x + dx, y + dy, z + dz, w + dw

                                    if clone.get(px, py, pz, pw) { activeCount++ }

                                    if activeCount > 3 { break outer }
                                }
                            }
                        }
                    }

                    if clone.get(x, y, z, w) {
                        if activeCount == 3 || activeCount == 2 { continue }

                        h.unset(x, y, z, w)
                        continue
                    }

                    if activeCount == 3 {
                        h.set(x, y, z, w)
                        continue
                    }
                }
            }
        }
    }
}

func (h hyper) count() int {
    count := 0
    minx, maxx, miny, maxy, minz, maxz, minw, maxw := h.size()

    for w := minw; w <= maxw; w++ {
        for z := minz; z <= maxz; z++ {
            for y := miny; y <= maxy; y++ {
                for x := minx; x <= maxx; x++ {
                    if h.get(x, y, z, w) { count++ }
                }
            }
        }
    }

    return count
}

func read(filename string) (hyper, error) {
    file, err := os.Open(filename)

    if err != nil { return nil, err }

    scanner := bufio.NewScanner(file)
    pl := plane{}
    y := 0

    for scanner.Scan() {
        ln := line{}

        for x, chr := range scanner.Text() {
            if chr != '#' { continue }

            ln[x] = '#'
        }

        pl[y] = ln
        y--
    }

    if err = scanner.Err(); err != nil { return nil, err }

    hy := hyper{}
    cu := cube{}
    cu[0] = pl
    hy[0] = cu
    return hy, nil
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
        return
    }

    hy, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    debug := false

    if len(os.Args) > 2 && os.Args[2] == "--print" {
        debug = true
    }

    if debug {
        fmt.Println("Before any cycles:")
        fmt.Println(hy.print())
    }

    for i := 0; i < 6; i++ {
        hy.cycle()

        if debug {
            fmt.Printf("\nAfter %d cycle:\n", i + 1)
            fmt.Println(hy.print())
        }
    }

    fmt.Println(hy.count())
}
