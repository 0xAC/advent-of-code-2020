package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "reflect"
    "regexp"
    "strconv"
    "strings"
)

type passport struct {
    BirthYear string `byr`
    IssueYear string `iyr`
    ExpirationYear string `eyr`
    Height string `hgt`
    HairColor string `hcl`
    EyeColor string `ecl`
    PassportID string `pid`
    CountryID string `cid`
}

func validate(count *int, line string) {
    p := parsePassport(line)

    if !p.IsValid() {
        return
    }

    *count += 1
}

func readFile(inputPath string) (int, error) {
    file, err := os.Open(inputPath)
    validCount := 0

    if err != nil {
        return validCount, err
    }

    defer file.Close()

    scanner := bufio.NewScanner(file)
    buffer := ""

    for scanner.Scan() {
        line := strings.Trim(scanner.Text(), " ")

        if len(line) > 0 {
            if len(buffer) > 0 {
                buffer += " "
            }

            buffer += line
            continue
        }

        validate(&validCount, buffer)
        buffer = ""
    }

    if len(buffer) > 0 {
        validate(&validCount, buffer)
    }

    if err = scanner.Err(); err != nil {
        return validCount, err
    }

    return validCount, nil
}

func parsePassport(line string) passport {
    var p passport

    fields := strings.Split(line, " ")

    for _, field := range fields {
        pieces := strings.Split(field, ":")
        name := pieces[0]
        value := pieces[1]

        p.SetField(name, value)
    }

    return p
}

func(p *passport) SetField(name string, value string) {
    elem := reflect.ValueOf(p).Elem()

    for i := 0; i < elem.NumField(); i++ {
        field := elem.Type().Field(i)
        
        if string(field.Tag) == name {
            elem.Field(i).SetString(value)
        }
    }
}

var numberRegex, _ = regexp.Compile("^\\d+$")
func validateNumber(value string, min int, max int) bool {
    if !numberRegex.MatchString(value) {
        return false
    }
    
    i, err := strconv.ParseInt(value, 10, 32)

    if err != nil {
        return false
    }

    if i < int64(min) {
        return false
    }

    if i > int64(max) {
        return false
    }

    return true
}

var yearRegex, _ = regexp.Compile("^\\d{4}$")
func validateYear(value string, min int, max int) bool {
    if !yearRegex.MatchString(value) {
        return false
    }

    return validateNumber(value, min, max)
}

var heightRegex, _ = regexp.Compile("^(?P<value>\\d+)(?P<unit>(in|cm))$")

func validateHeight(value string) bool {
    groups := heightRegex.FindStringSubmatch(value)

    if len(groups) < 3 {
        return false
    }

    unit := groups[2]
    number := groups[1]

    if unit == "cm" {
        return validateNumber(number, 150, 193)
    }

    return validateNumber(number, 59, 76)
}

var hairColorRegex, _ = regexp.Compile("^#[0-9a-f]{6}$")
func validateHairColor(value string) bool {
    return hairColorRegex.MatchString(value)
}

var eyeColorRegex, _ = regexp.Compile("^(amb|blu|brn|gry|grn|hzl|oth)$")
func validateEyeColor(value string) bool {
    return eyeColorRegex.MatchString(value)
}

var passportIdRegex, _ = regexp.Compile("^\\d{9}$")
func validatePassportId(value string) bool {
    return passportIdRegex.MatchString(value)
}

func(p *passport) IsValid() bool {
    if !validateYear(p.BirthYear, 1920, 2002) {
        return false
    }

    if !validateYear(p.IssueYear, 2010, 2020) {
        return false
    }

    if !validateYear(p.ExpirationYear, 2020, 2030) {
        return false
    }

    if !validateHeight(p.Height) {
        return false
    }

    if !validateHairColor(p.HairColor) {
        return false
    }

    if !validateEyeColor(p.EyeColor) {
        return false
    }

    if !validatePassportId(p.PassportID) {
        return false
    }

    return true
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input]\n", filepath.Base(os.Args[0]))
    }

    inputPath := os.Args[1]
    count, err := readFile(inputPath)

    if err != nil {
        log.Fatal(err)
    }

    fmt.Printf("%#v\n", count)
}
