package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "regexp"
    "strconv"
)

func read(filename string, mapper func(string) bool) (int, error) {
    file, err := os.Open(filename)
    count := 0

    if err != nil {
        return count, err
    }

    defer file.Close()

    scanner := bufio.NewScanner(file)

    for scanner.Scan() {
        if mapper(scanner.Text()) {
            count += 1
        }
    }

    if err := scanner.Err(); err != nil {
        return count, err
    }

    return count, nil
}

var exp, _ = regexp.Compile("^(\\d+)-(\\d+) (\\w): (\\w+)$")

func validate1(input string) bool {
    groups := exp.FindStringSubmatch(input)

    min, _ := strconv.ParseInt(groups[1], 10, 32)
    max, _ := strconv.ParseInt(groups[2], 10, 32)
    letter := []rune(groups[3])[0]

    count := int64(0)

    for _, c := range groups[4] {
        if c == letter {
            count += 1
        }
    }

    if (count < min) {
        return false
    }

    return !(count > max)
}

func boolToInt(val bool) int {
    if val {
        return 1
    }

    return 0
}

func intToBool(val int) bool {
    return val != 0
}

func validate2(input string) bool {
    groups := exp.FindStringSubmatch(input)

    idx1, _ := strconv.ParseInt(groups[1], 10, 32)
    idx2, _ := strconv.ParseInt(groups[2], 10, 32)
    letter := []rune(groups[3])[0]

    password := []rune(groups[4])

    fst := boolToInt(password[idx1-1] == letter)
    snd := boolToInt(password[idx2-1] == letter)

    return intToBool(fst ^ snd)
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input]\n", filepath.Base(os.Args[0]))
        return
    }

    count, err := read(os.Args[1], validate2)

    if err != nil {
        log.Fatal(err)
    }

    fmt.Println("count =", count)
}
