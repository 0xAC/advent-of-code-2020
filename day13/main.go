package main

import (
    "bufio"
    "fmt"
    "log"
    "math"
    "os"
    "path/filepath"
    "sort"
    "strconv"
    "strings"
)

func parseint(input string) (int, error) {
    val, err := strconv.ParseInt(input, 10, 64)

    if err != nil {
        return 0, err
    }

    return int(val), nil
}

var maxint = math.MaxInt64

func read(filename string) ([]string, error) {
    file, err := os.Open(filename)

    if err != nil {
        return nil, err
    }

    scanner := bufio.NewScanner(file)
    lines := []string{}

    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    if err = scanner.Err(); err != nil {
        return nil, err
    }

    return lines, nil
}

type table struct {
    ready int
    ids []int
}

func parsetable(line string) ([]int, error) {
    pieces := strings.Split(line, ",")
    ids := []int{}

    for _, piece := range pieces {
        if piece == "x" { piece = "0" }

        val, err := parseint(piece)

        if err != nil {
            return nil, err
        }

        ids = append(ids, val)
    }

    return ids, nil
}

func gcd2(a, b, o int) int {
    m := 1

    for {
        if (b * m) % a == o {
            return m
        }

        m++
    }
}

func gcd(a, b int) int {
    p := a
    q := b

    if a < b {
        p = b
        q = a
    }

    for {
        r := p % q

        if r == 0 {
            return q
        }

        p = q
        q = r
    }
}

func lcm(a, b int) int {
    return a * b / gcd(a, b)
}

func parse(lines []string) (*table, error) {
    t := table{}

    val, err := parseint(lines[0])

    if err != nil {
        return nil, nil
    }

    t.ready = val
    t.ids, err = parsetable(lines[1])

    if err != nil {
        return nil, err
    }

    return &t, nil
}

func solve1(t *table) int {
    minwait := maxint
    minid := 0

    for _, id := range t.ids {
        if id == 0 { continue }

        wait := id - (t.ready % id)

        if wait < minwait {
            minwait = wait
            minid = id
        }
    }

    return minid * minwait
}

func period(start int, step int, s map[int]int, last int) (int, int) {
    m := start - step
    first := 0

    outer:
    for {
        m += step

        for i, p := range s {
            if i > last { continue }
            if i == 0 { continue }

            if (m + i) % p != 0 { continue outer }
        }

        if first == 0 { first = m; continue }

        return first, m - first
    }
}

func main() {
    if len(os.Args) < 2 {
        fmt.Println("Usage: %s [input file]\n", filepath.Base(os.Args[1]))
        return
    }

    lines, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    tabl, err := parse(lines)

    if err != nil {
        log.Fatal(err)
    }

    s := map[int]int{}
    j := 0
    w := []int{}

    for i, k := range tabl.ids {
        if k == 0 { continue }
        w = append(w, i)

        s[i] = k

        if k > s[j] {
            j = i
        }
    }

    sort.Ints(w)

    x, y := period(0, s[w[0]], s, w[1])

    for _, j := range w[2:] {
        x, y = period(x, y, s, j)
    }

    fmt.Println(x)
}
