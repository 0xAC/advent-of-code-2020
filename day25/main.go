package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"unicode"
)

func readKeys(text string) (doorPubKey, cardPubKey int, err error) {
	pubKeysText := strings.FieldsFunc(string(text), func(c rune) bool { return !unicode.IsNumber(c) })

	if count := len(pubKeysText); count != 2 {
		log.Fatal(fmt.Errorf("Invalid input expected number of keys was 2 but got %d", count))
	}

	pubKeys := []int{}
	for _, pubKeyText := range pubKeysText {
		var value int64
		value, err = strconv.ParseInt(pubKeyText, 10, 64)

		if err != nil {
			return
		}

		pubKeys = append(pubKeys, int(value))
	}

	doorPubKey = pubKeys[0]
	cardPubKey = pubKeys[1]
	return
}

func findLoop(pubKey int) (loop int) {
	value := 1
	subject := 7
	i := 0

	for {
		i++
		value *= subject
		value %= 20201227

		if value == pubKey {
			break
		}
	}

	return i
}

func findKey(pubKey, loop int) int {
	value := 1

	for i := 0; i < loop; i++ {
		value *= pubKey
		value %= 20201227
	}

	return value
}

func hack(doorPubKey, cardPubKey int) int {
	return findKey(doorPubKey, findLoop(cardPubKey))
}

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
		return
	}

	text, err := ioutil.ReadFile(os.Args[1])

	if err != nil {
		log.Fatal(err)
	}

	doorPubKey, cardPubKey, err := readKeys(string(text))

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(hack(doorPubKey, cardPubKey))
}
