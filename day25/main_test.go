package main

import "testing"

func TestCardLoop(t *testing.T) {
	expLoop := 8

	loop := findLoop(5764801)

	if expLoop != loop {
		t.Errorf("Expected loop to be %d but got %d", expLoop, loop)
	}
}

func TestDoorLoop(t *testing.T) {
	expLoop := 11

	loop := findLoop(17807724)

	if expLoop != loop {
		t.Errorf("Expected loop to be %d but got %d", expLoop, loop)
	}
}

func TestDoorKey(t *testing.T) {
	key := findKey(17807724, 8)
	expKey := 14897079

	if expKey != key {
		t.Errorf("Expected encryption key to be %d but got %d", expKey, key)
	}
}

func TestCardKey(t *testing.T) {
	key := findKey(5764801, 11)
	expKey := 14897079

	if expKey != key {
		t.Errorf("Expected encryption key to be %d but got %d", expKey, key)
	}
}

func TestParseInput(t *testing.T) {
	doorPubKey, cardPubKey, err := readKeys("17807724\n5764801\n")

	if err != nil {
		t.Error(err)
		return
	}

	expDoorPubKey := 17807724
	expCardPubKey := 5764801

	if expDoorPubKey != doorPubKey {
		t.Errorf("Expected doorPubKey to be %d but got %d", expDoorPubKey, doorPubKey)
	}

	if expCardPubKey != cardPubKey {
		t.Errorf("Expected cardPubKey to be %d but got %d", expCardPubKey, cardPubKey)
	}
}

func TestHack(t *testing.T) {
	key := hack(17807724, 5764801)
	expKey := 14897079

	if expKey != key {
		t.Errorf("Expected key to be %d but got %d", expKey, key)
	}
}
