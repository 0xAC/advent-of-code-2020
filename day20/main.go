package main

import (
    "fmt"
    "log"
    "os"
    "path/filepath"
    "strconv"
    "strings"
)

type tile struct {
    id int
    pixels []rune
    width int
}

func parseInt(input string) (int, error) {
    val, err := strconv.ParseInt(input, 10, 64)

    return int(val), err
}

func (t tile) count(v rune) int {
    count := 0

    for y := 0; y < t.width; y++ {
        for x := 0; x < t.width; x++ {
            if t.get(x, y) != v { continue }

            count++
        }
    }

    return count
}

func (t tile) shrink() tile {
    clone := tile{}
    clone.id = t.id
    clone.width = t.width - 2
    clone.pixels = make([]rune, clone.width * clone.width)

    for x := 1; x < t.width - 1; x++ {
        for y := 1; y < t.width - 1; y++ {
            clone.set(x - 1, y - 1, t.get(x, y))
        }
    }

    return clone
}

func (t tile) hFlips() []tile {
    clone := t.clone()

    for y := 0; y < t.width; y++ {
        for x := 0; x < t.width; x++ {
            clone.set(x, t.width - 1 - y, t.get(x, y))
        }
    }

    return []tile{t, clone}
}

func (t tile) vFlips() []tile {
    clone := t.clone()

    for y := 0; y < t.width; y++ {
        for x := 0; x < t.width; x++ {
            clone.set(t.width - 1 - x, y, t.get(x, y))
        }
    }

    return []tile{t, clone}
}

func (t tile) rotateLeft() tile {
    clone := t.clone()

    for y := 0; y < t.width; y++ {
        for x := 0; x < t.width; x++ {
            clone.set(y, t.width - 1 - x, t.get(x, y))
        }
    }

    return clone
}

func (t tile) clone() tile {
    clone := tile{}
    clone.id = t.id
    clone.pixels = make([]rune, len(t.pixels))

    copy(clone.pixels, t.pixels)
    clone.width = t.width

    return clone
}

func (t tile) rotations() []tile {
    rotations := []tile{t}

    rotated90 := t.rotateLeft()
    rotations = append(rotations, rotated90)
    rotated180 := rotated90.rotateLeft()
    rotations = append(rotations, rotated180)
    rotations = append(rotations, rotated180.rotateLeft())

    return rotations
}

func (t tile) permutations() []tile {
    permutations := []tile{}

    for _, hFlip := range t.hFlips() {
        for _, vFlip := range hFlip.vFlips() {
            permutations = append(permutations, vFlip.rotations()...)
        }
    }

    return permutations
}

func (t tile) String() string {
    lines := []string{fmt.Sprintf("Tile %d:", t.id)}

    for i := 0; i < len(t.pixels); i += t.width {
        lines = append(lines, string(t.pixels[i:i+t.width]))
    }

    lines = append(lines, "")

    return strings.Join(lines, "\n")
}

func (t tile) get(x, y int) rune {
    return t.pixels[y * t.width + x]
}

func (t tile) set(x, y int, v rune) {
    t.pixels[y * t.width + x] = v
}

func pixelToBit(r rune) int {
    if r == '#' { return 1 }

    return 0
}

func (t tile) scanEdgeH(y int) int {
    side1 := 0
    side2 := 0

    for i := 0; i < t.width; i++ {
        j := t.width - 1 - i

        if i > 0           { side1 <<= 1 }
        if j < t.width - 1 { side2 <<= 1 }

        side1 |= pixelToBit(t.get(i, y))
        side2 |= pixelToBit(t.get(j, y))
    }

    highSide := 0
    lowSide := 0

    if side1 < side2 {
        highSide = side1
        lowSide = side2
    } else {
        highSide = side2
        lowSide = side1
    }

    return (highSide << t.width) | lowSide
}

func (t tile) scanEdgeV(x int) int {
    side1 := 0
    side2 := 0

    for i := 0; i < t.width; i++ {
        j := t.width - 1 - i

        if i > 0           { side1 <<= 1 }
        if j < t.width - 1 { side2 <<= 1 }

        side1 |= pixelToBit(t.get(x, i))
        side2 |= pixelToBit(t.get(x, j))
    }

    highSide := 0
    lowSide := 0

    if side1 < side2 {
        highSide = side1
        lowSide = side2
    } else {
        highSide = side2
        lowSide = side1
    }

    return (highSide << t.width) | lowSide
}

func (t tile) topSideId() int {
    return t.scanEdgeH(0)
}

func (t tile) bottomSideId() int {
    return t.scanEdgeH(t.width - 1)
}

func (t tile) leftSideId() int {
    return t.scanEdgeV(0)
}

func (t tile) rightSideId() int {
    return t.scanEdgeV(t.width - 1)
}

func (t tile) sides() []int {
    sides := []int{}

    sides = append(sides, t.topSideId())
    sides = append(sides, t.bottomSideId())
    sides = append(sides, t.leftSideId())
    sides = append(sides, t.rightSideId())

    return sides
}

func mapIdsBySides(tiles map[int]tile) map[int][]int {
    idsBySide := map[int][]int{}

    for _, ti := range tiles {
        for _, s := range ti.sides() {
            sides, ok := idsBySide[s]

            if !ok { sides = []int{} }

            idsBySide[s] = append(sides, ti.id)
        }
    }

    return idsBySide
}

func findFreeSidesById(tiles map[int]tile) map[int][]int {
    idsBySide := mapIdsBySides(tiles)

    freeSidesById := map[int][]int{}

    for side, ids := range idsBySide {
        if len(ids) == 1 {
            it, ok := freeSidesById[ids[0]]

            if !ok { it = []int{} }

            freeSidesById[ids[0]] = append(it, side)
        }
    }

    return freeSidesById
}

func coreners(tiles map[int]tile) ([]int, error) {
    freeSidesById := findFreeSidesById(tiles)
    cornerIds := []int{}

    for id, sides := range freeSidesById {
        if len(sides) != 2 { continue }

        cornerIds = append(cornerIds, id)
    }

    if len(cornerIds) != 4 {
        return nil, fmt.Errorf("Misidentified corners foud %d corners", len(cornerIds))
    }

    return cornerIds, nil
}

type pair struct {
    fst, snd int
}

func findConnections(tiles map[int]tile) []pair {
    idsBySide := mapIdsBySides(tiles)
    pairs := []pair{}

    for _, ids := range idsBySide {
        if len(ids) == 1 { continue }

        head := ids[0]

        for _, id := range ids[1:] {
            pairs = append(pairs, pair{fst: head, snd: id})
        }
    }

    return pairs
}

func includes(stack []int, needle int) bool {
    for _, item := range stack {
        if needle == item { return true }
    }

    return false
}

func findTopLeft(tiles map[int]tile, cornersIds []int) tile {
    freeSidesById := findFreeSidesById(tiles)
    id := cornersIds[0]
    ti := tiles[id]

    freeSideIds := freeSidesById[id]

    for _, p := range ti.permutations() {
        if !includes(freeSideIds, p.topSideId()) { continue }
        if !includes(freeSideIds, p.leftSideId()) { continue }

        return p
    }

    panic("This should never happen")
}

func findOnRight(tiles map[int]tile, onRight tile, onTop tile, idsBySide map[int][]int) tile {
    sideId := onRight.rightSideId()
    candidateIds, ok := idsBySide[sideId]

    if !ok { panic(fmt.Sprintf("Cannot find a tile with side %d", sideId)) }

    for _, candidateId := range candidateIds {
        if candidateId == onRight.id { continue }

        other, ok := tiles[candidateId]

        if !ok { continue }

        for _, p := range other.permutations() {
            if p.leftSideId() == sideId {
                if onTop.id == 0 {
                    if len(idsBySide[p.topSideId()]) == 1 { return p }

                    continue
                }

                if onTop.bottomSideId() == p.topSideId() { return p }
            }
        }
    }

    panic(fmt.Sprintf("Could not find sibling of tile with id: %d", onRight.id))
}

func findOnBottom(tiles map[int]tile, onTop tile, idsBySide map[int][]int) tile {
    sideId := onTop.bottomSideId()
    candidateIds, ok := idsBySide[sideId]

    if !ok { panic(fmt.Sprintf("Cannot find a tile with side %d", sideId)) }

    for _, candidateId := range candidateIds {
        if candidateId == onTop.id { continue }

        other, ok := tiles[candidateId]

        if !ok { continue }

        for _, p := range other.permutations() {
            if p.topSideId() == sideId {
                if len(idsBySide[p.leftSideId()]) != 1 { continue }

                return p
            }
        }
    }

    panic(fmt.Sprintf("Could not find sibling of tile with id: %d", onTop.id))
}

func findGrid (tiles map[int]tile) [][]tile {
    idsBySide := mapIdsBySides(tiles)
    cornerIds, err := coreners(tiles)
    if err != nil { log.Fatal(err) }
    topLeft := findTopLeft(tiles, cornerIds)

    row := findRow(tiles, topLeft, idsBySide, 0, nil)
    rows := [][]tile{row}
    count := len(tiles) - len(row)

    for count > 0 {
        leftMost := findOnBottom(tiles, rows[len(rows)-1][0], idsBySide)
        row = findRow(tiles, leftMost, idsBySide, len(rows[0]), rows[len(rows)-1])
        rows = append(rows, row)
        count -= len(row)
    }

    return rows
}

func findRow(tiles map[int]tile, leftMost tile, idsBySide map[int][]int, limit int, prevRow []tile) []tile {
    row := []tile{leftMost}

    cornerIds, err := coreners(tiles)

    if err != nil { log.Fatal(err) }
    i := 1

    for {
        onTop := tile{}

        if prevRow != nil {
            onTop = prevRow[i]
        }

        ti := findOnRight(tiles, row[len(row) - 1], onTop, idsBySide)

        row = append(row, ti)
        i++

        if limit == 0 {
            if  includes(cornerIds, ti.id) { break }
        } else {
            if len(row) == limit { break }
        }
    }

    return row
}

func assemblyImage(pieces [][]tile) tile {
    image := tile{}
    image.width = 0

    for _, c := range pieces[0] {
        image.width += c.width - 2
    }

    image.pixels = make([]rune, image.width * image.width)

    shrinked := [][]tile{}

    for _, r := range pieces {
        row := []tile{}

        for _, t := range r {
            row = append(row, t.shrink())
        }

        shrinked = append(shrinked, row)
    }

    for i, r := range shrinked {
        for j, t := range r {
            x := j * t.width
            y := i * t.width

            for ty := 0; ty < t.width; ty++ {
                for tx := 0; tx < t.width; tx++ {
                    image.set(x + tx, y + ty, t.get(tx, ty))
                }
            }
        }
    }

    return image
}

type monster struct {
    width, length int
    pixels []rune
}

func (m monster) set(x, y int, v rune) {
    m.pixels[y * m.width + x] = v
}

func (m monster) get(x, y int) rune {
    return m.pixels[y * m.width + x]
}

func createMonster() monster {
    lines := [][]rune{
        []rune(string("                  # ")),
        []rune(string("#    ##    ##    ###")),
        []rune(string(" #  #  #  #  #  #   ")),
    }

    mon := monster{}
    mon.length = len(lines)
    mon.width = len(lines[0])
    mon.pixels = make([]rune, mon.width * mon.length)

    for y, line := range lines {
        for x, chr := range line {
            mon.set(x, y, chr)
        }
    }

    return mon
}

func detectMonster(image tile, mon monster) int {
    total := 0
    for y := 0; y < image.width - mon.length; y++ {
        for x := 0; x < image.width - mon.width; x++ {
            found := true
            power := 0

            for dy := 0; dy < mon.length; dy++ {
                for dx := 0; dx < mon.width; dx++ {
                    if mon.get(dx, dy) != '#' { continue }

                    if image.get(x + dx, y + dy) != '#' {
                        found = false
                        break
                    }

                    power++
                }
            }

            if found { total += power }
        }
    }

    return total
}

func findMonster(image tile) int {
    mon := createMonster()

    for _, perm := range image.permutations() {
        power := detectMonster(perm, mon)

        if power > 0 {
            return image.count('#') - power
        }
    }

    return 0
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
        return
    }

    tiles, err := read(os.Args[1])

    if err != nil { log.Fatal(err) }

    cornerIds, err := coreners(tiles)

    if err != nil { log.Fatal(err) }

    cornerProduct := 1
    for _, id := range cornerIds {
        cornerProduct *= id
    }

    pieces := findGrid(tiles)

    product := 1
    for i, r := range pieces {
        for j, t := range r {
            if (i == 0 || i == len(pieces) - 1) && (j == 0 || j == len(pieces) - 1) {
                product *= t.id
            }
        }
    }

    fmt.Println("part 1", product)
    fmt.Println("part 2", findMonster(assemblyImage(pieces)))
}
