package main

import (
    "bufio"
    "fmt"
    "os"
    "regexp"
)

type reader struct {
    idRegexp *regexp.Regexp
    rowRegexp *regexp.Regexp
}

func (r *reader) readTile(scanner *bufio.Scanner) (tile, error) {
    groups := r.idRegexp.FindStringSubmatch(scanner.Text())
    ti := tile{width: 10}

    if groups == nil {
        return ti, fmt.Errorf("Unexpected line '%s'", scanner.Text())
    }

    id, err := parseInt(groups[1])
    if err != nil { return ti, err }

    ti.id = id

    for scanner.Scan() {
        line := scanner.Text()

        if !r.rowRegexp.MatchString(line) { break }

        ti.pixels = append(ti.pixels, []rune(line)...)
    }

    if len(ti.pixels) < 100 {
        return ti, fmt.Errorf("Found a tile with %d pixels", len(ti.pixels))
    }

    return ti, nil
}

func (r *reader) read(filename string) (map[int]tile, error) {
    file, err := os.Open(filename)

    if err != nil {
        return nil, err
    }

    scanner := bufio.NewScanner(file)
    tiles := map[int]tile{}

    for scanner.Scan() {
        if scanner.Text() == "" {continue }

        ti, err := r.readTile(scanner)

        if err != nil {
            return nil, err
        }

        tiles[ti.id] = ti
    }

    if err := scanner.Err(); err != nil {
        return nil, err
    }

    return tiles, nil
}

func read(filename string) (map[int]tile, error) {
    idRegexp, err := regexp.Compile("^Tile (\\d+):$")

    if err != nil { return nil, err }

    rowRegexp, err := regexp.Compile("^[#\\.]{10}$")

    if err != nil { return nil, err }

    rdr := &reader{idRegexp, rowRegexp}

    return rdr.read(filename)
}
