package main

import(
    "bufio"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "regexp"
    "sort"
    "strconv"
    "strings"
)

func read(filename string) ([]string, error) {
    file, err := os.Open(filename)

    if err != nil {
        return nil, err
    }

    scanner := bufio.NewScanner(file)
    lines := []string{}

    for scanner.Scan() {
        line := scanner.Text()

        if line == "" { continue }

        lines = append(lines, line)
    }

    if err = scanner.Err(); err != nil {
        return nil, err
    }
    
    return lines, nil
}

type span struct {
    min, max int
}

type rule struct {
    name string
    spans []span
}

func (r rule) valid(value int) bool {
    for _, span := range r.spans {
        if span.min <= value && span.max >= value { return true }
    }

    return false
}

type data struct {
    nearbyTickets [][]int
    ownTicket []int
    rules []rule
}

func parseInt(number string) (int, error) {
    value, err := strconv.ParseInt(number, 10, 64)

    if err != nil {
        return 0, err
    }

    return int(value), nil 
}

func parseRule(name string, spansText string) (rule, error) {
    r := rule{name: name, spans: []span{}}

    for _, text := range strings.Split(spansText, " or ") {
        numbers := strings.Split(text, "-")

        min, err := parseInt(numbers[0])

        if err != nil {
            return r, err
        }

        max, err := parseInt(numbers[1])

        if err != nil {
            return r, err
        }

        r.spans = append(r.spans, span{min: min, max: max})
    }

    return r, nil
}

func parseRules(i int, lines []string) ([]rule, int, error) {
    ruleRegexp, err := regexp.Compile("^([^:]+): (\\d+-\\d+( or \\d+-\\d+)*)$")

    if err != nil {
        return nil, i, err
    }

    rules := []rule{}

    for {
        if i >= len(lines) { break }

        groups := ruleRegexp.FindStringSubmatch(lines[i])

        if groups == nil { break }

        r, err := parseRule(groups[1], groups[2])

        if err != nil { return nil, i, err }

        rules = append(rules, r)

        i++
    }

    if len(rules) == 0 {
        return rules, i, fmt.Errorf("Empty list of rules")
    }

    return rules, i, nil
}

func parseNumbers(line string) ([]int, error) {
    fieldsRegexp, err := regexp.Compile("^(\\d+)(,\\d+)*$")

    if err != nil {
        return nil, err
    }

    if !fieldsRegexp.MatchString(line) {
        return nil, fmt.Errorf("Unexpected line %s", line)
    }

    numbers := []int{}

    for _, text := range strings.Split(line, ",") {
        value, err := parseInt(text)

        if err != nil {
            return nil, err
        }

        numbers = append(numbers, value)
    }

    return numbers, nil
}

func parse(lines []string) (*data, error) {
    d := data{}
    var err error

    i := 0

    d.rules, i, err = parseRules(i, lines)

    if err != nil { return nil, err }

    if lines[i] != "your ticket:" {
        return nil, fmt.Errorf("Missing 'your ticket' section, got '%s'", lines[i])
    }

    i++

    d.ownTicket, err = parseNumbers(lines[i])

    if err != nil {
        return nil, err
    }

    if len(d.ownTicket) != len(d.rules) {
        return nil, fmt.Errorf("Got %d rules but %d values in %s", len(d.rules), len(d.ownTicket), lines[i])
    }

    i++

    if lines[i] != "nearby tickets:" {
        return nil, fmt.Errorf("Missing 'nearby tickets' section")
    }

    i++

    for _, line := range lines[i:] {
        ticket, err := parseNumbers(line)

        if err != nil {
            return nil, err
        }

        if len(ticket) != len(d.rules) {
            return nil, fmt.Errorf("Got %d rules but %d values in %s", len(d.rules), len(ticket), line)
        }

        d.nearbyTickets = append(d.nearbyTickets, ticket)
    }

    return &d, nil
}

func (d *data) excludeInvalid() {
    validTickets := [][]int{}

    for _, ticket := range d.nearbyTickets {
        ticketValid := true

        for _, t := range ticket {
            fieldValid := false

            for _, r := range d.rules {
                if !r.valid(t) { continue }

                fieldValid = true
                break
            }

            if !fieldValid { ticketValid = false }
        }

        if ticketValid {
            validTickets = append(validTickets, ticket)
        }
    }

    d.nearbyTickets = validTickets
}

type candidate struct {
    options []int
    name string
}

type candidates []candidate

func (cs candidates) Len() int {
    return len(cs)
}

func (cs candidates) Swap(i, j int) {
    cs[i], cs[j] = cs[j], cs[i]
}

func (cs candidates) Less(i, j int) bool {
    return len(cs[i].options) < len(cs[j].options)
}

func (d *data) createFieldMatchCandidates() candidates {
    candidates := candidates{}

    for _, rule := range d.rules {
        cand := candidate{name: rule.name}

        for i, _ := range d.ownTicket {
            ok := true

            for _, t := range d.nearbyTickets {
                if rule.valid(t[i]) { continue }

                ok = false
                break
            }

            if !ok { continue }

            cand.options = append(cand.options, i)
        }

        candidates = append(candidates, cand)
    }

    sort.Sort(candidates)

    return candidates
}

func (cs candidates) resolve() (map[string]int, error) {
    usedCache := map[int]bool{}
    result := map[string]int{}

    for _, cand := range cs {
        for _, field := range cand.options {
            if _, used := usedCache[field]; used { continue }

            usedCache[field] = true
            result[cand.name] = field
            break
        }

        if _, ok := result[cand.name]; !ok {
            return nil, fmt.Errorf("Cannot find match for field named '%s'", cand.name)
        }
    }

    return result, nil
}

func (d *data) find() (map[string]int, error) {
    result := map[string]int{}

    fieldIndicesMap, err := d.createFieldMatchCandidates().resolve()

    if err != nil {
        return nil, err
    }

    for name, index := range fieldIndicesMap {
        result[name] = d.ownTicket[index]
    }

    return result, nil
}

func productFieldsWithPrefix(input map[string]int, prefix string) int {
    product := 1

    for name, value := range input {
        if !strings.HasPrefix(name, prefix) { continue }

        product *= value
    }

    return product
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s [input file]\n", filepath.Base(os.Args[0]))
        return
    }

    lines, err := read(os.Args[1])

    if err != nil {
        log.Fatal(err)
    }

    dat, err := parse(lines)

    if err != nil {
        log.Fatal(err)
    }

    dat.excludeInvalid()

    record, err := dat.find()

    if err != nil {
        log.Fatal(err)
    }

    fmt.Println(productFieldsWithPrefix(record, "departure"))
}

