package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
)

func read(filename string, mapper func(string) int) (int, int, int, error) {
    file, err := os.Open(filename)

    if err != nil {
        return 0, 0, 0, nil
    }

    defer file.Close()

    scanner := bufio.NewScanner(file)

    sum_ := 0
    min_ := 1024
    max_ := 0

    for scanner.Scan() {
        val := mapper(scanner.Text())

        min_ = min(min_, val)
        max_ = max(max_, val)
        sum_ += val
    }

    if err := scanner.Err(); err != nil {
        return 0, 0, 0, nil
    }

    return min_, max_, sum_, nil
}

func decode(code string) int {
    rowCode := code[0:7]
    colCode := code[7:]

    row := 0
    for _, c := range rowCode {
        row <<= 1

        if c == 'B' {
            row |= 1
        }
    }

    col := 0
    for _, c := range colCode {
        col <<= 1

        if c == 'R' {
            col |= 1
        }
    }

    return row * 8 + col
}

func min(a int, b int) int {
    if a < b {
        return a
    }

    return b
}

func max(a int, b int) int {
    if a > b {
        return a
    }

    return b
}

func gauss(a int) int {
    return a * (a + 1) / 2
}

func main() {
    min_, max_, sum_, err := read(os.Args[1], decode)

    if err != nil {
        log.Fatal(err)
    }

    val := gauss(max_) - sum_ - gauss(min_ - 1)

    fmt.Println("val =", val)
}
